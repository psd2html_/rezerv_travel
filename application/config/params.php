<?php

return [
    'adminEmail' => 'admin@example.com',
    'roles' => [
        'superadmin'    => 'SuperAdmin',
        'admin'         => 'Admin',
        'manager'       => 'Manager',
    ],

    'thumbnails' => [
        'width' => 100,
        'height' => 100
    ]

//    'mdm.admin.configs' => [
//        'defaultUserStatus' => 0, // 0 = inactive, 10 = active
//    ]

];
