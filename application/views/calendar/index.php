<div id="chess" class="chess">
    <div id="chess_sidebar" class="chess-sidebar">
        <div class="row top date-row">
            <div class="cell w100">
                <div class="date-input" id="calendar_date">
                </div>
                <div id="date_switch" class="date-switch" data-action="chess">
                    <div id="datepicker_show" class="switch date" data-date="20.06.2017"><b>20</b> июн</div>
                    <div id="calendar_today" class="switch today active">Сегодня</div>
                    <div id="calendar_arrows" class="arrows">
                        <a href="#" class="button prev"><i class="icon icon-prev"></i></a>
                        <a href="#" class="button next"><i class="icon icon-next"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row title">
            <div class="cell w100">
                <a href="#" class="cat" title="НОМЕР DOUBLE (двухместный номер с 1 кроватью)" data-cat="id_cat_1">Номер "DOUBLE" (двухместный номер с 1 кроватью) <i class="icon icon-down"></i><i class="icon icon-up"></i></a>
            </div>
        </div>

        <div class="rooms" data-cat="id_cat_1">
            <div class="row" data-room="101">
                <div class="cell w100">
                    <div class="num">101</div>
                </div>
            </div>
            <div class="row" data-room="102">
                <div class="cell w100">
                    <div class="num">102</div>
                </div>
            </div>
            <div class="row" data-room="103">
                <div class="cell w100">
                    <div class="num">103</div>
                </div>
            </div>
            <div class="row" data-room="104">
                <div class="cell w100">
                    <div class="num">104</div>
                </div>
            </div>
            <div class="row" data-room="105">
                <div class="cell w100">
                    <div class="num">105</div>
                </div>
            </div>
            <div class="row" data-room="106">
                <div class="cell w100">
                    <div class="num">106</div>
                </div>
            </div>
            <div class="row" data-room="107">
                <div class="cell w100">
                    <div class="num">107</div>
                </div>
            </div>
            <div class="row" data-room="108">
                <div class="cell w100">
                    <div class="num">108</div>
                </div>
            </div>
            <div class="row" data-room="109">
                <div class="cell w100">
                    <div class="num">109</div>
                </div>
            </div>
            <div class="row" data-room="110">
                <div class="cell w100">
                    <div class="num">110</div>
                </div>
            </div>
        </div>

        <div class="row title">
            <div class="cell w100">
                <a href="#" class="cat" data-cat="id_cat_2" title="Полулюкс">Полулюкс <i class="icon icon-down"></i><i class="icon icon-up"></i></a>
            </div>
        </div>

        <div class="rooms" data-cat="id_cat_2">
            <div class="row" data-room="201">
                <div class="cell w100">
                    <div class="num">201</div>
                </div>
            </div>
            <div class="row" data-room="202">
                <div class="cell w100">
                    <div class="num">202</div>
                </div>
            </div>
            <div class="row" data-room="203">
                <div class="cell w100">
                    <div class="num">203</div>
                </div>
            </div>
            <div class="row" data-room="204">
                <div class="cell w100">
                    <div class="num">204</div>
                </div>
            </div>
            <div class="row" data-room="205">
                <div class="cell w100">
                    <div class="num">205</div>
                </div>
            </div>
            <div class="row" data-room="206">
                <div class="cell w100">
                    <div class="num">206</div>
                </div>
            </div>
            <div class="row" data-room="207">
                <div class="cell w100">
                    <div class="num">207</div>
                </div>
            </div>
            <div class="row" data-room="208">
                <div class="cell w100">
                    <div class="num">208</div>
                </div>
            </div>
            <div class="row" data-room="209">
                <div class="cell w100">
                    <div class="num">209</div>
                </div>
            </div>
            <div class="row" data-room="210">
                <div class="cell w100">
                    <div class="num">210</div>
                </div>
            </div>
        </div>

        <div class="row title">
            <div class="cell w100">
                <a href="#" class="cat" data-cat="id_cat_3" title="Люкс">Люкс <i class="icon icon-down"></i><i class="icon icon-up"></i></a>
            </div>
        </div>

        <div class="rooms" data-cat="id_cat_3">
            <div class="row" data-room="301">
                <div class="cell w100">
                    <div class="num">301</div>
                </div>
            </div>
            <div class="row" data-room="302">
                <div class="cell w100">
                    <div class="num">302</div>
                </div>
            </div>
            <div class="row" data-room="303">
                <div class="cell w100">
                    <div class="num">303</div>
                </div>
            </div>
            <div class="row" data-room="304">
                <div class="cell w100">
                    <div class="num">304</div>
                </div>
            </div>
            <div class="row" data-room="305">
                <div class="cell w100">
                    <div class="num">305</div>
                </div>
            </div>
        </div>
    </div>

    <div id="chess_content" class="chess-content divided">

        <div id="scroll_content" class="scroll-content" style="">
            <div id="calendar_dates" class="row top">

            </div>

            <div id="rooms_wrapper" class="rooms-wrapper load">
                <div class="row title" data-cat="id_cat_1">

                </div>

                <div class="rooms" data-cat="id_cat_1">
                    <div class="row" data-room="101"></div>
                    <div class="row" data-room="102"></div>
                    <div class="row" data-room="103"></div>
                    <div class="row" data-room="104"></div>
                    <div class="row" data-room="105"></div>
                    <div class="row" data-room="106"></div>
                    <div class="row" data-room="107"></div>
                    <div class="row" data-room="108"></div>
                    <div class="row" data-room="109"></div>
                    <div class="row" data-room="110"></div>
                </div>

                <div class="row title" data-cat="id_cat_2"></div>

                <div class="rooms" data-cat="id_cat_2">
                    <div class="row" data-room="201"></div>
                    <div class="row" data-room="202"></div>
                    <div class="row" data-room="203"></div>
                    <div class="row" data-room="204"></div>
                    <div class="row" data-room="205"></div>
                    <div class="row" data-room="206"></div>
                    <div class="row" data-room="207"></div>
                    <div class="row" data-room="208"></div>
                    <div class="row" data-room="209"></div>
                    <div class="row" data-room="210"></div>
                </div>

                <div class="row title" data-cat="id_cat_3"></div>

                <div class="rooms" data-cat="id_cat_3">
                    <div class="row" data-room="301"></div>
                    <div class="row" data-room="302"></div>
                    <div class="row" data-room="303"></div>
                    <div class="row" data-room="304"></div>
                    <div class="row" data-room="305"></div>
                </div>

                <div id="bookings_wrapper">
                    <!--<div class="booking in early later" data-id="id1" data-cat="1" data-room="101" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 120px; top: 35px; left: 240px;">
                        <span class="text">Малчановский Александр <i class="wave left"></i><i class="wave right"></i></span>
                    </div>

                    <div class="booking out" data-id="id2" data-cat="1" data-room="102" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 120px; top: 70px; left: 120px;">
                        <span class="text" >Малчановский Александр <i class="wave left"></i><i class="wave right"></i></span>
                    </div>

                    <div class="booking confirmed" data-id="id3" data-cat="1" data-room="103" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 240px; top: 105px; left: 0px;">
                        <span class="text">Малчановский Александр <i class="wave left"></i><i class="wave right"></i></span>
                    </div>

                    <div class="booking not-confirmed" data-id="id4" data-cat="1" data-room="104" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 240px; top: 140px; left: 120px;">
                        <span class="text">Малчановский Александр <i class="wave left"></i><i class="wave right"></i></span>
                    </div>

                    <div class="booking in wave-right" data-id="id5" data-cat="1" data-room="105" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 120px; top: 140px; left: 480px;">
                        <span class="text">Малчановский Александр <i class="wave right"></i><i class="wave right"></i></span>
                    </div>

                    <div class="booking in wave-left" data-id="id6" data-cat="1" data-room="105" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 120px; top: 175px; left: 600px;">
                        <span class="text">Малчановский Александр <i class="wave left"></i><i class="wave right"></i></span>
                    </div>

                    <div class="booking reserved" data-id="id7" data-cat="1" data-room="103" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 240px; top: 105px; left: 360px;">
                        <span class="text">Резерв <i class="wave left"></i><i class="wave right"></i></span>
                    </div>

                    <div class="booking blocked" data-id="id7" data-cat="1" data-room="104" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 240px; top: 105px; left: 720px;">
                        <span class="text">Заблокировано <i class="wave left"></i><i class="wave right"></i></span>
                    </div>-->
                </div>

            </div>

            <div id="highlighting_day" class="highlighting-day"></div>

            <div id="timeline" class="timeline"></div>
        </div>
    </div>
</div>


