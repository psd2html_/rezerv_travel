<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BathroomType */

$this->title = Yii::t('app', 'Create Bathroom Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bathroom Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bathroom-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
