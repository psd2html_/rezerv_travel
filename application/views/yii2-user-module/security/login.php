<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign in');
?>

<?php //echo $this->render('/_alert', ['module' => Yii::$app->getModule('user')]); ?>

<!--модальное окно авторизации-->
<div class="modal auth">
    <div class="modal-content medium">
        <p class="caption align-center"><?= Html::encode($this->title) ?></p>

        <?php
            $form = ActiveForm::begin([
                'id' => 'auth_form',
//                'fieldConfig' => [
//                    'options' => [
//                        'tag' => false,
//                    ],
//                ],
                'options' => [
                    'class' => 'form mt-15',
                ],
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]);
        ?>

            <label class="field w100">
                <?php if ($module->debug) : ?>
                    <?= $form->field($model, 'login', [
                        'inputOptions' => [
                            'autofocus' => 'autofocus',
                            'class' => 'form-control validate',
                            'tabindex' => '1']])->dropDownList(LoginForm::loginList());
                    ?>
                <?php else: ?>
                    <?=
                        $form->field($model, 'login',
                            [
                                'inputOptions' =>
                                    [
                                        'autofocus'     => 'autofocus',
                                        'placeholder'   => 'Логин',
                                        'class'         => 'form-control validate',
                                        'data-validate' => 'required',
                                        'tabindex'      => '1',
                                    ],
                                'options' => [
                                    'tag' => null, // Don't wrap with "form-group" div
                                ],
                                'template' => '{input}{error}{hint}',
                            ]
                        )->label(false);
                    ?>
                <?php endif ?>
            </label>


            <label class="field w100">
                <?php if ($module->debug): ?>
                    <div class="alert alert-warning">
                        <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
                    </div>
                <?php else: ?>
                    <?= $form->field(
                        $model,
                        'password',
                        [
                            'inputOptions' => ['class' => 'form-control validate', 'placeholder' => 'Пароль', 'tabindex' => '2', 'data-validate' => 'required'],
                            'template' => '{input}{error}{hint}',
                        ])
                        ->passwordInput()
                        ->label(false);
                    ?>
                <?php endif ?>
            </label>

            <div class="info">
                    <?=
                        $form->field($model, 'rememberMe',
                            [
                                'options' => [
                                    'tag' => null, // Don't wrap with "form-group" div
                                ],
                            ]
                        )
                            ->checkbox(
                                [
                                    'labelOptions' => ['class' => 'field mt-0 float-left checkbox'],

                                ],
                                true);
                    ?>



                <?=
                    Html::a(
                        '<i class="demo-icon icon-user"></i><span class="text">' . Yii::t('user', 'Register'). '</span>',
                        ['/user/register'],
                        ['tabindex' => '5', 'class' => 'link float-right']
                    );
                ?>
                <div class="clr"></div>
                <?=
                    Html::a(
                        '<i class="icon icon-lock"></i><span class="text">' . Yii::t('user', 'Forgot your password?'). '</span>',
                        ['/user/recovery/request'],
                        ['tabindex' => '6', 'class' => 'link float-right mb-15']
                    );
                ?>


                <div class="clr"></div>
            </div>

            <div class="buttons mt-0">
                <?=
                    Html::submitButton(
                        Yii::t('user', 'Log In'),
                        ['class' => 'button-form w100 ok', 'id' => 'auth_submit', 'tabindex' => '4']
                    )
                ?>
            </div>



        <?php ActiveForm::end(); ?>
    </div>
</div>