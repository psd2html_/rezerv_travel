<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logs".
 *
 * @property string $id
 * @property string $create_at
 * @property string $user_id
 * @property string $event
 * @property string $comments
 * @property string $username
 */
class Logs extends \yii\db\ActiveRecord
{

    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_at'], 'safe'],
            [['comments'], 'string'],
            [['event'], 'string', 'max' => 255],
            [['user_id'], 'integer', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'create_at' => Yii::t('app', 'Create at'),
            'user_id' => Yii::t('app', 'user_id'),
            'event' => Yii::t('app', 'event'),
            'comments' => Yii::t('app', 'comments'),
            'username' => Yii::t('app', 'username'),
        ];
    }


    public function getUsername()
    {
        $user = Yii::$app->user->identity;
        return $user ? Yii::$app->user->identity->username : '';
    }
}
