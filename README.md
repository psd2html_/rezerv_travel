Project: 'RezervTravel'
============================
#### PHP: 5.6+
#### Framework: Yii 2.* basic
#### Database: MySQL 5.7+


INSTALLATION
------------
#### Clone project
~~~
git clone https://[YOUR_ACCOUNT_ON_BITBUCKET]@bitbucket.org/psd2html_/rezerv_travel.git
~~~

#### Composer. In project folder
~~~
php composer.phar install
~~~

DIRECTORY STRUCTURE
-------------------

      application/assets/             contains assets definition      
      application/commands/           contains console commands (controllers)
	  application/config/             contains application configurations      
      application/controllers/        contains web controller classes      
      application/mail/               contains view files for e-mails
      application/models/             contains model classes
      application/runtime/            contains files generated during runtime
	  application/tests/              contains various tests for the basic application
	  application/vendor/             contains dependent 3rd-party packages
	  application/views/              contains view files for the Web application
	  application/web/                contains the entry script and Web resources
      application/web/assets          contains web assets
      application/web/css             contains styles folder
      application/web/js              contains java-script folder
	  application/web/image           contains images	  
	  
Apache2 config
-------------------
~~~
<VirtualHost *:%httpport%>
    DocumentRoot    "%hostdir%/[PATH_TO_WEB]/web/"
    ServerName      "%host%"
    ...
</VirtualHost>
~~~