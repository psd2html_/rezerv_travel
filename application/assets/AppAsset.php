<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/normalize.css',
        'css/jquery-ui.css',
        'css/jscrollpane.css',
        'css/waves.css',
        'css/sweetalert.css',
        'css/jquery.dataTables.min.css',
        'css/bootstrap-select.min.css',
        'css/daterangepicker.css',
        'css/forms.css',
        'css/style.css',
        'css/main-custom.css',
    ];
    
    public $js = [
        'js/modernizr-2.6.2-respond-1.1.0.js',
        'js/waves.js',
        'js/jquery.formstyler.js',
        'js/jquery.jscrollpane.js',
        'js/jquery.moment.js',
        'js/jquery.mousewheel.js',
        'js/jquery.ui.punch.js',
        'js/jquery_maska.js',
        'js/sweetalert.min.js',
        'js/bootstrap.min.js',
        'js/bootstrap-select.min.js',
        'js/jquery.sortElements.js',
        'js/daterangepicker.js',
        'js/form.js',
        'js/script.js?v=1',
    ];
    
    
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
