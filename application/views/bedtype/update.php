<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BedType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Bed Type',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bed Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bed-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
