<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BathroomType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Bathroom Type',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bathroom Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bathroom-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
