<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 03.07.2017
 * Time: 14:14
 */

namespace app\components;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class RController extends Controller
{
    public function beforeAction($action)
    {

        if (parent::beforeAction($action))
        {
            if (!\Yii::$app->user->isGuest)
            {
                $controller = ucfirst(\Yii::$app->controller->id);
                $action = $action->id;
                $access = $controller.'.'.$action;

                if (!\Yii::$app->user->can($access))
                    throw new ForbiddenHttpException('Access denied.');
                else
                    return true;
            }
        } else {
            return false;
        }
    }
}