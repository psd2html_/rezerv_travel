<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "hotels".
 *
 * @property string $id
 * @property string $name
 * @property integer $type
 * @property integer $stars
 * @property string $address
 * @property string $contact_phone
 * @property string $mobile_phone
 * @property string $sms_send
 * @property string $fax
 * @property string $fax_send
 * @property string $contact_email
 * @property string $notify_email
 * @property string $text
 * @property string $image
 * @property string $logo
 */
class Hotels extends \yii\db\ActiveRecord
{
    const TYPE_OTEL     = 1;
    const TYPE_HOSTEL   = 2;

    const STATUS_SMS_SEND_FALSE = 0; // не отправлено
    const STATUS_SMS_SEND_TRUE = 1; // отправлено

    const STATUS_FAX_SEND_FALSE = 0; // не отправлено
    const STATUS_FAX_SEND_TRUE = 1; // отправлено

    const STATUS_NOTIFY_EMAIL_FALSE = 0; // не отправлено
    const STATUS_NOTIFY_EMAIL_TRUE = 1; // отправлено

    public $file;
    public $file_logo;
    public $del_img;
    public $del_img_logo;




    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'stars', 'sms_send', 'fax_send', 'notify_email'], 'integer'],
            [['text'], 'string'],
            [['name', 'address', 'contact_phone', 'mobile_phone', 'fax', 'contact_email', 'logo'], 'string', 'max' => 255],

            [['file', 'file_logo'], 'file', 'extensions' => 'png, jpg'],
            [['del_img', 'del_img_logo'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'stars' => 'Stars',
            'address' => 'Address',
            'contact_phone' => 'Contact Phone',
            'mobile_phone' => 'Mobile Phone',
            'sms_send' => 'Sms Send',
            'fax' => 'Fax',
            'fax_send' => 'Fax Send',
            'contact_email' => 'Contact Email',
            'notify_email' => 'Notify Email',
            'text' => 'Text',
            'image' => 'Image',
            'logo' => 'Logo',
        ];
    }

    /**
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type, $code = NULL)
    {
        $_items = array(
            'Type' => [
                self::TYPE_HOSTEL => 'Хостел',
                self::TYPE_OTEL   => 'Отель',
            ],
            'StatusSmsSend' => [
                self::STATUS_SMS_SEND_FALSE => Yii::t('app', 'not send'),
                self::STATUS_SMS_SEND_TRUE => Yii::t('app', 'send'),
            ],
            'StatusFaxSend' => [
                self::STATUS_FAX_SEND_FALSE => Yii::t('app', 'not send'),
                self::STATUS_FAX_SEND_TRUE => Yii::t('app', 'send'),
            ],
            'StatusNotifyEmail' => [
                self::STATUS_NOTIFY_EMAIL_FALSE => Yii::t('app', 'not send'),
                self::STATUS_NOTIFY_EMAIL_TRUE => Yii::t('app', 'send'),
            ],
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }


    public static function getImageThumbs($id, $image)
    {
        $thumb_w = Yii::$app->params['thumbnails']['width'];
        $thumb_h = Yii::$app->params['thumbnails']['height'];

        $dir = Yii::getAlias('@images'). '/hotels/' . $id . '/thumbs/';
        return Html::img('/'. $dir . $thumb_w .'_'. $thumb_h . '_' . $image);
    }
}
