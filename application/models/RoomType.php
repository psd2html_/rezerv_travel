<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "room_type".
 *
 * @property string $id
 * @property string $hotel_id
 * @property string $name
 * @property double $square
 * @property string $bed_type_id
 * @property string $bathroom_type_id
 * @property string $view_room_type_id
 *
 * @property Hotels $hotel
 * @property BedType $bedType
 * @property BathroomType $bathroomType
 * @property ViewRoomType $viewRoomType
 */
class RoomType extends \yii\db\ActiveRecord
{

    public $hotelname;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id'], 'required'],
            [['hotel_id', 'bed_type_id', 'bathroom_type_id', 'view_room_type_id'], 'integer'],
            [['square'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotels::className(), 'targetAttribute' => ['hotel_id' => 'id']],
            [['bed_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BedType::className(), 'targetAttribute' => ['bed_type_id' => 'id']],
            [['bathroom_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BathroomType::className(), 'targetAttribute' => ['bathroom_type_id' => 'id']],
            [['view_room_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ViewRoomType::className(), 'targetAttribute' => ['view_room_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'hotel_id' => Yii::t('app', 'Hotel ID'),
            'name' => Yii::t('app', 'Категория'),
            'square' => Yii::t('app', 'Площадь номера'),
            'bed_type_id' => Yii::t('app', 'Тип кровати'),
            'bathroom_type_id' => Yii::t('app', 'Тип ванной'),
            'view_room_type_id' => Yii::t('app', 'Вида из номера'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotels::className(), ['id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBedType()
    {
        return $this->hasOne(BedType::className(), ['id' => 'bed_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBathroomType()
    {
        return $this->hasOne(BathroomType::className(), ['id' => 'bathroom_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewRoomType()
    {
        return $this->hasOne(ViewRoomType::className(), ['id' => 'view_room_type_id']);
    }



}
