<?php

namespace app\helpers;

use Yii;
use app\models;
use yii\db\Expression;

class H_Logs
{

    /**
     * Логирование действий пользователя
     *
     * @param $event - событие
     * @param $comments - доп. комментарий
     */
    public static function AddLog($event, $comments = '')
    {
        $modelLogs              = new models\Logs();
        $modelLogs->user_id     = \Yii::$app->user->identity->id;
        $modelLogs->event       = $event;
        $modelLogs->comments    = $comments;
        $modelLogs->create_at   = new Expression('NOW()');
        $modelLogs->save();
    }
}