$(document).ready(function(){

    $('.selectpicker').selectpicker();

    $('.date-rangepicker').each(function () {
        initDateRangePicker($(this));
    });

    $('.selectpicker-multi').each(function () {
        initMultiSelector($(this));
    });

    $('.date-rangepicker').on('showCalendar.daterangepicker', function(ev, picker) {
        $('.calendar.left').find('.calendar-table .table-condensed thead tr:first-child th:last-child').addClass('next');
    });

    // Сортировка таблиц
    $('.sortable').each(function(){
        var table = $(this).parent().parent().parent();
        var th = $(this),
            thIndex = th.index(),
            inverse = false;
        th.click(function(){
            if($(this).hasClass('sort-asc')){
                $(this).removeClass('sort-asc').addClass('sort-desc');
            }else{
                if($(this).hasClass('sort-desc')){
                    $(this).removeClass('sort-desc').addClass('sort-asc');
                }else{
                    table.find('thead td').removeClass('sort-asc sort-desc');
                    $(this).addClass('sort-asc');
                }
            }
            if(!$(this).hasClass('sort-asc') && !$(this).hasClass('sort-desc')){
                $(this).addClass('sort-asc')
            }
            table.find('tbody td').filter(function(){
                return $(this).index() === thIndex;
            }).sortElements(function(a, b){
                return $.text([a]) > $.text([b]) ? inverse ? -1 : 1 : inverse ? 1 : -1;
            }, function(){
                return this.parentNode;
            });
            inverse = !inverse;
        });
    });


    //инициализация маски ввода
    maskedInput();

    //placeholders
    //initPlaceholders();

    //tooltip
    $('document').tooltip({
        track: true
    });

	getViewport();

    //показ/закрытие меню
    $('#sidebar_toggle').on('click', function(){
        if ($('body').hasClass('sidebar-open')) {
            $('body').removeClass('sidebar-open');

        } else {
            $('body').addClass('sidebar-open');
        }
        return false;
    });

    //эффект при клке
    Waves.init();
    Waves.attach('.waves-effect', null, true);

    // Новое бронирование
    if($('#add-new-booking-page').length){

        // Для теста
        var fio= [
            {
                'name':'Петров',
                'phone':'9999999999',
                'email':'qwe@qwe.er'
            },
            {
                'name':'Сидоров',
                'phone':'8888888888',
                'email':'te@qwe.er'
            },
            {
                'name':'Якубович',
                'phone':'7777777777',
                'email':'dfge@qwe.er'
            },
            {
                'name':'Жигалойдов',
                'phone':'6666666666',
                'email':'fghfth@qwe.er'
            },
            {
                'name':'Коперник',
                'phone':'5555555555',
                'email':'jghft@qwe.er'
            }
        ];
        $('.autocomplete.fio').autocomplete({
            source: $.map(fio, function (item) {
                return item.name;
            }),
            select: function (e, ui) {
                for(var i = 0; i < fio.length; i++){
                    if(fio[i].name == ui.item.value){
                        $('.wide-sidebar .field.phone input').val(fio[i].phone);
                        $('.wide-sidebar .field.email input').val(fio[i].email);
                    }
                }
            }
        });

        //добавление нового бронирования
        function initFormAddNewBooking() {
            var $form = $('#add-new-booking-form');
            $form.find('.validate').on('change', function(){
                validateField($(this));
            });

            //отправка данных
            $('#add-new-booking-submit').on('click', function(){
                if (validateForm($form)) {
                 $form.addClass('load');
                 //добавление брони
                 var data = $form.serialize();
                 //добавляем бронь
                 //chessSendAjaxAddBooking(data);
                 }
                 return false;
            });

        }
        initFormAddNewBooking();

        $('#search-apartment').on('submit', function (e) {
            e.preventDefault();
        });

        $('.checkbox-search input').on('change', function () {
            var inputs = $('.checkbox-search input:checked'),
                apartment = $('.apartment');
            if(inputs.length > 0){
                var inputsOptions = [];
                for(var i = 0; i < inputs.length; i++){
                    inputsOptions.push($(inputs[i]).attr('id'));
                }
                for(i = 0; i < apartment.length; i++){
                    var bodyInfo = $(apartment[i]).find('.body-info'),
                        count = 0;
                    for(var j = 0; j < bodyInfo.length; j++){
                        var dataOptions = $(bodyInfo[j]).data('options').split(',');
                        if(contains(dataOptions,inputsOptions)){
                            console.log('yes');
                            count++;
                            $(bodyInfo[j]).show();
                        }else{
                            $(bodyInfo[j]).hide();
                        }
                    }
                    if(count == 0){
                        $(apartment[i]).hide();
                    }else{
                        $(apartment[i]).show();
                    }
                }
            }else{
                for(i = 0; i < apartment.length; i++){
                    $(apartment[i]).show();
                    $(apartment[i]).find('.body-info').show();
                }
            }
        });

        // Проверяет входит 1 массив в другой или нет
        function contains(where, what) {
            if (!what) { // условие #1
                return true;
            }
            for (var i = 0; i < what.length; i++) {
                for (var j = 0; j < where.length; j++) {
                    if (what[i] == where[j]) {
                        break;
                    }
                    if (j === where.length - 1) {
                        // мы дошли до конца массива, и так и не нашли вхождение - значит, у нас есть элемент, который не входит в where, и нужно вернуть false
                        return false;
                    }
                }
            }
            // ни для одного из элементов не сработал return false, а значит, все они найдены
            return true;
        }
    }

    //шахматка
    if ($('#chess').length) {
        //номера
        $chessWrapper = $('#rooms_wrapper');
        //бронирования
        $chessBookings = $('#bookings_wrapper');
        

        //свернуть/развернуть категорию номера
        $('#chess .cat').on('click', function(){
            var cat = $(this).attr('data-cat');
            if ($(this).hasClass('rooms-hidden')) {
                $(this).removeClass('rooms-hidden');
                $('#chess .rooms[data-cat="'+cat+'"]').show();
                $('#bookings_wrapper .booking[data-cat="'+cat+'"]').show();
            } else {
                $(this).addClass('rooms-hidden');
                $('#chess .rooms[data-cat="'+cat+'"]').hide();
                $('#bookings_wrapper .booking[data-cat="'+cat+'"]').hide();
            }

            chessReinitDroppable();
            chessRefreshPositionBookings();
            return false;
        }); 

        //при клике по fade удаляем новые несозданные брони
        $('body').on('click', '#fade_dialog_booking', function(){
            chessCloseDialogBooking();
            return false;
        });

        //показ/закрытие нераспределенных бронирований!
        $chessWrapper.on('click', '.count.link', function(){

            //категория
            var cat = $(this).attr('data-cat');
            var $objRooms = $chessWrapper.children('.rooms[data-cat="'+cat+'"]');
            


            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('body').removeClass('sidebar-right-open');

                //удаляем drag/drop
                chessSidebarDestroyDragDrop();
                $objRooms.find('.sidebar-drop').remove();

            } else {
                
                $chessWrapper.find('.count.active').removeClass('active');
                $(this).addClass('active');

                //если сайдбар был скрыт
                //смещаем скролл на ширину сайдбара
                if (!$('body').hasClass('sidebar-right-open')) {
                    var leftScroll = $('#chess_content').scrollLeft();
                    var leftCell = $(this).parent().position().left + CHESS_WIDTH_CELL+325;
                    var chessWidth = $('#chess_content').width() * 1;

                    //проверяем попадает ли выделенная ячейка в область просмотра
                    //если нет, тогда смещаем скролл на величину сайдбара
                    var max = leftScroll + chessWidth;
                    if (leftCell > max) {
                        $('#chess_content').scrollLeft(leftScroll+325);
                    }
                }
                $('body').addClass('sidebar-right-open');

                //выбранная дата
                var date = $(this).parent().attr('data-date');
                
                //день недели и месяц
                var dateArr = date.split('.');
                var dateMoment = moment([dateArr[2],dateArr[1]-1,dateArr[0]]);
                var dateText = WEEKDAYS[dateMoment.format('E')-1]+', '+dateMoment.format('D');

                //название категории
                var $objCat = $('#chess_sidebar .cat[data-cat="'+cat+'"]');
                var catName = $objCat.attr('title');
                //если номера категории скрыты - показываем их!
                if ($objCat.hasClass('rooms-hidden')) {
                    $objCat.trigger('click');
                }

                //формируем html сайдбара
                $('#sidebar_right_category').html(catName);
                $('#sidebar_right_date').html(dateText);

                //удаляем старые принимающие ячейки
                $chessWrapper.find('.sidebar-drop').remove();
                //добавляем принимающие ячейки для выбранной категории и инициализируем их
                $objRooms.find('.cell[data-date="'+date+'"]').append('<div class="sidebar-drop"></div>');
                chessSidebarInitDroppable();
                
                //получаем бронирования
                chessGetNewBookings(date, cat);
            }

            return false;
        });

        //закрытие из сайдбара!
        $('#close_sidebar_right_chess').on('click', function(){
            $('body').removeClass('sidebar-right-open');
            $chessWrapper.find('.count.active').removeClass('active');
            //удаляем drag/drop
            chessSidebarDestroyDragDrop();
            $chessWrapper.find('.sidebar-drop').remove();

            return false;
        });

        //инициализация управление календарем
        //для раздела шахматки
        initDateSwitch('chess');
        
        //инициализация календаря
        calendarInit('', chessRefresh);


        //диалоговое окно добавления бронирования
        $('#dialog_add_booking')
            .on('click', '#booking_block', function(){
                //показ формы добавления блокировки
                var $booking = $chessBookings.find('.new');
                chessShowModalReserveBlock($booking, 'blocked');

                return false;  
            })
            .on('click', '#booking_reserve', function(){
                //показ формы добавления резерва
                var $booking = $chessBookings.find('.new');
                chessShowModalReserveBlock($booking, 'reserved');

                return false;  
            });

        //модальное окно добавление блокировки/резерва
        function initFormAddReserveBlock() {
            var $form = $('#reserve_block_form');
            $form.find('.validate').on('change', function(){
                validateField($(this));
            });

            //отправка данных
            $('#reserve_block_button_submit').on('click', function(){
                if (validateForm($form)) {
                    $form.addClass('load');
                    //добавление брони
                    var data = $form.serialize();
                    //добавляем бронь
                    chessSendAjaxAddBooking(data);

                }
                return false;
            });

            //отмена/закрытие мод.окна
            $('#reserve_block_button_cancel').on('click', function(){
                //закрываем мод.окно
                chessCloseModalReserveBlock();

                return false;
            });

        }
        initFormAddReserveBlock();
    }  


    //тарифы
    if ($('#rates').length) {
        //свернуть/развернуть категорию номера
        $('#rates .cat').on('click', function(){
            if (!$(this).hasClass('no-clickable')) {
                var cat = $(this).attr('data-cat');
                if ($(this).hasClass('rooms-hidden')) {
                    $(this).removeClass('rooms-hidden');
                    $('#rates .rates[data-cat="'+cat+'"]').show();
                } else {
                    $(this).addClass('rooms-hidden');
                    $('#rates .rates[data-cat="'+cat+'"]').hide();
                }
            }
            return false;
        });

        //закрытие из сайдбара!
        $('#close_sidebar_right_rates').on('click', function(){
            $('body').removeClass('sidebar-right-open');

            return false;
        });

        //показ сайдбара с тарифами
        $('#btn_rates').on('click', function(){
            if ($('body').hasClass('sidebar-right-open')) {
                $('body').removeClass('sidebar-right-open');

            } else {
                $('body').addClass('sidebar-right-open');
            }

            return false;
        });

        //инициализация управление календарем
        //для раздела шахматки
        initDateSwitch('rates');
        
        //инициализация календаря
        calendarInit('', ratesRefresh);

        //инициализация sortable
        $('#rates_active, #rates_disabled').sortable({
            'connectWith': '.rates-sortable',
            'axis': 'y'
        }).disableSelection();


        //выбор тарифа
        $('#rate_select').on('change', function(){
            var val = $(this).val();
            if (val == 'all') { 
                val = '';
                $('#button_change_rate').addClass('disable');

            } else {
                $('#button_change_rate').removeClass('disable');
            }
            ratesGetPrices(val);
        });

        //отмена изменений порядка тарифов в сайдбаре
        $('#rates_change_cancel').on('click', function(){
            $('body').removeClass('sidebar-right-open');
            return false;
        });

        //применение изменений порядка тарифов в сайдбаре
        $('#rates_change_submit').on('click', function(){
            var rate = $('#rate_select').val();
            if (rate == 'all') { rate = ''; }
            ratesGetPrices(rate);

            $('body').removeClass('sidebar-right-open');
            return false;
        });
    }


    //форма авторизации
    function initAuthForm() {
        var $form = $('#auth_form');
        $form.find('.validate').on('change', function(){
            validateField($(this));
        });

        //отправка данных
        $('#auth_submit').on('click', function(){
            if (validateForm($form)) {
                return true;
                
            } else {
                return false;    
            }
            
        });
    }
    if ($('#auth_form').length) {
        initAuthForm();
    }

    //если открыто мод.окно запрещаем скролл
    $(window).on('mousewheel', function(event) {
        if ($('.modal:visible').length) {
            return false;
        }
    });

    $(window).on('resize', function(event) {
        getViewport();

        //позиционирование диалогового окна добавления броней
        if ($('#dialog_add_booking:visible').length) {
            var $booking = $chessBookings.children('.booking.new');
            chessSetPositionDialogBooking($booking)
        }
        
    });

});

// Склонение слов по падежам в зависимости от количесва
// Пример:
// declOfNum(term, ['неделя', 'недели', 'недель'])
function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

//Инициализация мультиселектора
function initMultiSelector($elem) {
    // скрываем селекс, чтобы глаз не мозолил
    $elem.css({"display":"none"});
    // генерирую обёртку + шапку, которую будет видеть поль-ль
    var container = $('<div />',{
        'class': 'selectpicker-multi form-control'
    }).append(
        $('<span />',{
            'text': getTitle(),
            'class': 'selectpicker-multi-text'
        })
    );
    // генерирую контент, который будет выпадать
    var content = $('<div />',{
        'class': 'selectpicker-multi-content'
    });
    var options = $elem.find('option');
    for(var i = 0; i < options.length; i++){
        var contentElem = $('<div />');
        var minusButton = $('<div />',{
            'class': 'minus-button',
            'text': '–'
        });
        // получение значения из атрибута value. Если его нет, то ставим по умолчанию 0
        // так же проверяем, если валуе больше минимального значения, то даем возможность уменьшать валуе
        var val = 0;
        if($(options[i]).val()){
            val = Number($(options[i]).val());
            if($(options[i]).data('minvalue')){
                if(Number($(options[i]).data('minvalue')) < val){
                    minusButton.addClass('clickable');
                }
            }
        }
        var plusButton = $('<div />',{
            'class': 'plus-button clickable',
            'text': '+'
        });
        // тут собираю строку (один спан из селекта = одной строке здесь)
        contentElem.append(minusButton);
        contentElem.append(
            $('<div />').append(
                $('<span />',{
                    'text': val,
                    'class': 'selectpicker-multi-val'
                })
            ).append(
                $('<span />',{
                    'text': ' ' + declOfNum($(options[i]).val(), $(options[i]).data('text').split(','))
                })
            )
        );
        contentElem.append(plusButton);
        content.append(contentElem);

        // навешиваю обработчики событий для кнопки минус
        minusButton.on('click', function () {
            if($(this).hasClass('clickable')){
                var el = $(this).next().find('.selectpicker-multi-val'),
                    minValue = 0,
                    index = $(this).parent().index(),
                    contentElem = $(this).prev().find('span:last-child');
                if(options.eq(index).data('minvalue')){
                    minValue = Number(options.eq(index).data('minvalue'));
                }
                options.eq(index).val(Number(el.text()) - 1);
                contentElem.text(' ' + declOfNum(options.eq(index).val(), options.eq(index).data('text').split(',')));
                el.text(Number(el.text()) - 1);
                if(Number(el.text()) == minValue){
                    $(this).removeClass('clickable');
                }
                container.find('.selectpicker-multi-text').text(getTitle());
            }
        });
        // навешиваю обработчики событий для кнопки плюс
        plusButton.on('click', function () {
            var el = $(this).prev().find('.selectpicker-multi-val'),
                index = $(this).parent().index(),
                contentElem = $(this).prev().find('span:last-child');
            options.eq(index).val(Number(el.text()) + 1);
            contentElem.text(' ' + declOfNum(options.eq(index).val(), options.eq(index).data('text').split(',')));
            el.text(Number(el.text()) + 1);
            $(this).prev().prev().addClass('clickable');
            container.find('.selectpicker-multi-text').text(getTitle());
        });
    }

    // добавляю сгенерированную обёртку в дом, сразу после исходного селекта
    container.append(content);
    $elem.after(container);

    // сворачивание и открывание контента при клике по шапке
    container.find('.selectpicker-multi-text').on('click', function (e) {
       content.toggle();
    });

    function getTitle() {
        var text = '',
            options = $elem.find('option');
        for(var i = 0; i < options.length; i++){
            text += $(options[i]).val() + ' ' + declOfNum($(options[i]).val(), $(options[i]).data('text').split(',')) + ', ';
        }
        if(text.length > 1){
            text = text.substring(0, text.length - 2);
        }
        return text;
    }
}

//инициализация календаря выбора периода
function initDateRangePicker($elem) {
    var format = 'DD.MM.YYYY',
        startDate = moment(),
        endDate = moment().add(1, 'days');
    if($elem.data('format')){
        format = $elem.data('format');
    }
    if($elem.data('start')){
        startDate = moment($elem.data('start'), format);
    }
    if($elem.data('end')){
        endDate = moment($elem.data('end'), format);
    }
    if(startDate > endDate){
        endDate = moment(startDate);
        endDate.add(1, 'days');
    }
    $elem.daterangepicker(
        {
            'autoApply': true,
            'autoUpdateInput':true,
            'locale':{
                'format': format,
                'daysOfWeek': WEEKDAYS_SHORT_DATEPICKER,
                'monthNames': MONTHS,
                'firstDay': 1,
                'showOtherMonths': true,
                'selectOtherMonths': true
            },
            'startDate': startDate,
            'endDate': endDate
        }
    );
}

var viewportWidth,
    viewportHeight;
//ф-я получения размеров вьюпорта
function getViewport() {
    viewportWidth  = $(window).width() * 1;

    viewportHeight = $(window).height() * 1;
    if (viewportHeight < 700) {
        viewportHeight = 700;
    }
}

//ф-я показа модального окна
//на вход html
function showModalMessage(html) {
}

//текущая дата
var TODAY_DATE = new Date();
var TODAY_DAY = TODAY_DATE.getDate();
if (TODAY_DAY < 10) { TODAY_DAY = '0'+TODAY_DAY; }

var TODAY_MONTH = TODAY_DATE.getMonth() + 1;
if (TODAY_MONTH < 10) { TODAY_MONTH = '0'+TODAY_MONTH; }
var TODAY_YEAR = TODAY_DATE.getFullYear();
var TODAY_HOUR = TODAY_DATE.getHours();

//месяцы
var MONTHS      = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
var MONTHS_SHORT = ['янв','фев','мар','апр','мая','июн','июл','авг','сен','окт','ноя','дек'];

//дни недели
var WEEKDAYS = ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
//для ui datepicker
var WEEKDAYS_SHORT_DATEPICKER = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
var WEEKDAYS_DATEPICKER = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

/* формирование календаря и ячеек*/
//на вход дата ДД.ММ.ГГГГ относительно которой идёт формирование
//и фукнция которая вызовется после успешного формирования даты
function calendarInit(date, callback) {
    //прелоадер на контент
    $('#rooms_wrapper').addClass('load');

    //max кол-ва выводимых месяцев
    var countMonth = 3;

    //если дата не указана - берем текущую
    if (date == undefined || date == '') {
        var dateMoment = moment();

    } else {
        var dateArr = date.split('.');
        var dateMoment = moment([dateArr[2], [dateArr[1]-1], 1]);
    }

    moment.locale('ru');

    var day   = 1; 
    var month = dateMoment.format('M'); 
    var year  = dateMoment.format('gggg');

    //отсчет начинаем с предыдущего месяца
    var dateMin = dateMoment.subtract(15, 'days');
    var dayMin = dateMin.format('D')
    var monthMin = dateMin.format('M');
    var yearMin = dateMin.format('gggg');

    //если минимальный месяц и текущий совпадают
    //уменьшаем кол-во выводимых месяцев
    var flagMonth = 0; 
    if (monthMin == month) {
        countMonth = 2;
        flagMonth = 1;
    }

    //console.log(monthMin);

    //HTML календаря
    var calendarHTML = '';
    //HTML ячеек
    var cellsHTML = '';

    //ширина контента
    var width = 0;

    //текущая дата
    //var now = moment();
    var today = moment([TODAY_YEAR,TODAY_MONTH,TODAY_DAY]);
    //формирование HTML для календаря и ячеек
    function createHTML(day, month, year) {
        var startDate = day;

        if (countMonth > 0) {
            var monthDays = moment(year+'-'+month, 'YYYY-M').daysInMonth();
            if (countMonth == 1 && flagMonth != 1) {
                monthDays = 15;
            }

            var monthHTML = '';

            while (day <= monthDays) {
                var weekday = moment(year+'-'+month+'-'+day,'YYYY-M-D').format('E');

                var addClass = '';                  
                //отмечаем первый день месяца
                if (day == 1) {
                    addClass += ' first';
                }

                //отмечаем прошедшие даты
                //текущая дата
                var cur = moment([year,month,day]);
                //разница между сегодня и текущей датой
                var diff = today.diff(cur);
                
                var archiveClass = '';
                if (diff > 0) {
                    archiveClass += ' archive';
                }


                var weekendClass = '';
                if (weekday == 6 || weekday ==7) {
                    weekendClass = 'weekend';
                }

                var dayFull = day;
                if (day < 10) {
                    dayFull = '0'+dayFull;
                }

                var monthFull = month;
                if (month < 10) {
                    monthFull = '0'+monthFull;
                }

                var fullDate = dayFull+'.'+monthFull+'.'+year;
                var textDate = WEEKDAYS[weekday-1]+', '+day;
                //если первый день месяца - добавляем название месяца к выводимой дате!
                if (day == 1) { 
                    textDate = textDate+' '+MONTHS_SHORT[month-1];
                }


                calendarHTML += '<div title="'+fullDate+'" class="cell '+addClass+'" data-date="'+fullDate+'"><div class="date '+weekendClass+'">'+textDate+'</div></div>';
                cellsHTML += '<div class="cell '+archiveClass+'" data-date="'+fullDate+'"></div>';

                day++;
                width = width + CHESS_WIDTH_CELL;
            }

            //$calendar.append(monthHTML);

            countMonth--;
            month = month*1+1;

            if (month > 12) {
                month = 1;
                year++;
            }

            createHTML(1, month, year);

        } else {
            //добавляем HTML
            $('#calendar_dates').html(calendarHTML);
            $('#rooms_wrapper .row').html(cellsHTML);

            //задаем ширину контента
            $('#scroll_content').css('width', width);

            //убираем прелоадер
            $('#rooms_wrapper').removeClass('load');

            //вызываем callback ф-ю
            if (callback !=undefined && callback != '') {
                callback();
            }

            //устанавливаем дату
            setCalendarDate(date);
        }
    }
    createHTML(dayMin, monthMin, yearMin);
}
/* END формирование календаря и ячеек для шахматки*/


//подстветка ячеек выбранной пользователем даты
//на вход дата в формате ДД.ММ.ГГГГ
function setCalendarDate(date) {
    if (date == undefined || date == '') {
        date = TODAY_DAY+'.'+TODAY_MONTH+'.'+TODAY_YEAR;
    }
    //console.log(date);
    var dateArr = date.split('.');
    $('#datepicker_show').attr('data-date', date).html('<b>'+dateArr[0]+'</b> '+MONTHS_SHORT[dateArr[1]-1]);

    //обновляем значение datepickera
    $('#datepicker').datepicker('setDate'. date);
    
    //устанавливаем смещения для подсветки текущей даты
    var $date = $('#calendar_dates .cell[data-date="'+date+'"]');
    if ($date.length) {
        var left = $date.position().left;
        $('#highlighting_day').css('left', left);
        //проскролливаем к текущей дате
        $('#chess_content').scrollLeft(left-119);
    }

    setPositionTimeline();
}

//линия с текущем временем
function setPositionTimeline() {
    if ($('#timeline').length) {
        var $date = $('#calendar_dates .cell[data-date="'+TODAY_DAY+'.'+TODAY_MONTH+'.'+TODAY_YEAR+'"]');
        //если текущий день есть на странице
        if ($date.length) {
            var left = $date.position().left;
            var leftTimeline = Math.round(left + TODAY_HOUR/24*CHESS_WIDTH_CELL);
            $('#timeline').css('left', leftTimeline);
        } else {
            $('#timeline').css('left', '-1000px');
        }
    } 
}

//инициализация управления календарем
//на вход page - страница для которой запускаем!
function initDateSwitch(page) {
    //получаем action для ф-и переключения даты
    var $dateSwitch = $('#date_switch');
    var action = $dateSwitch.attr('data-action');

    //обертка для datepicker
    var $datepicker = $('#calendar_date');
    //инициализация datepicker
    $datepicker.datepicker({
        'firstDay': 1,
        'dateFormat': 'dd.mm.yy',
        'dayNames': WEEKDAYS_DATEPICKER,
        'dayNamesMin': WEEKDAYS_SHORT_DATEPICKER,
        'monthNames': MONTHS,
        'prevText': '',
        'nextText': '',
        beforeShow: function(input, inst) {
            
        },
        onSelect: function(dateText, inst) {
            $('#datepicker_show').addClass('active').siblings('.active').removeClass('active');
            changeDate(dateText);
        }
    });
    $('#datepicker_show').on('click', function(){
        if ($datepicker.filter(':visible').length) {
            $datepicker.hide();

        } else {
            $datepicker.datepicker('setDate', $('#datepicker_show').attr('data-date')).show();

        }
        
        return false;
    });

    //изменение даты - на вход новая дата ДД.ММ.ГГГГ!
    function changeDate(date) {
        //скрываем datepicker
        $datepicker.hide();

        //проверяем дату - если на вход даты нет, значит сегодняшняя!
        if (date == undefined || date == '') {
            date = TODAY_DAY+'.'+TODAY_MONTH+'.'+TODAY_YEAR;
        }

        //получаем объект с выбранной датой
        //если объект найден, переходим к выбранном дню
        //иначе в зависимости от того на какой странице находимся вызываем ф-ю переинициализации календаря!
        var $objDate = $('#calendar_dates .cell[data-date="'+date+'"]');
        if ($objDate.length) {
            setCalendarDate(date);

        } else {
            //для шахматки
            if (action == 'chess') {
                calendarInit(date, chessRefresh);  
            }
            //для тарифов
            if (action == 'rates') {
                calendarInit(date, ratesRefresh);
            }
        }
    }

    //клик по сегодня
    $('#calendar_today').on('click', function(){
        $(this).addClass('active').siblings('.active').removeClass('active');
        changeDate();
    });

    //переключение по стрелкам
    $('#calendar_arrows .button').on('click', function(){
        //получаем текущую дату
        var dateArr = $('#datepicker_show').attr('data-date').split('.');
        var date = moment([dateArr[2],dateArr[1]-1,dateArr[0]]);
        
        if ($(this).hasClass('next')) {
            date = date.add(1, 'days');
        } else {
            date = date.subtract(1, 'days');
        }
        
        date = date.format('DD.MM.YYYY');
        changeDate(date);

        //делаем активным кнопку с календарём
        $('#datepicker_show').addClass('active').siblings('.active').removeClass('active');
        
        return false;
    });
}

//шахматка
var $chessWrapper,
    $chessBookings;

//ширина ячейки шахматки
var CHESS_WIDTH_CELL = 120;

//получение бронирований, резервов, блокировок
//статусы бронирований: 
//in - проживает, out - выехал,
//confirmed - подтверждено, not-confirmed - не подтверждено
//reserved - резерв, blocked - заблокировано
function chessGetBookings() {
    chessClearBookings();
    //получаем период
    var $dates = $('#calendar_dates').children('.cell');
    var start = $dates.first().attr('data-date');
    var finish = $dates.last().attr('data-date');

    $.ajax({
        url      : '/ajax/',
        type     : 'POST',
        dataType : 'json',
        data     : {
            'period': start+'-'+finish
        },
        timeout  : 60000,
        success: function(res){
            chessParseBookings(res);
        },
        error: function(res){
            var res = {
                'id1': {
                    'datein'  : '01.07.2017', //заезд
                    'dateout' : '04.07.2017', //выезд
                    'nights'  : '3', //ночей
                    'room'    : '102', //id/номер номера
                    'cat'     : 'id_cat_1', //категория номера,
                    'fio'     : 'Фамилия1 Имя1', //фио
                    'status'  : 'in', //статус бронирования
                    'early'   : false, //ранний заезд
                    'later'   : false //поздний выезд
                },
                'id2': {
                    'datein'  : '22.06.2017',
                    'dateout' : '23.06.2017',
                    'nights'  : '1',
                    'room'    : '104',
                    'cat'     : 'id_cat_1',
                    'fio'     : 'Фамилия2 Имя2',
                    'status'  : 'out',
                    'early'   : true,
                    'later'   : true
                },
                'id3': {
                    'datein'  : '22.06.2017',
                    'dateout' : '25.06.2017',
                    'nights'  : '3',
                    'room'    : '201',
                    'cat'     : 'id_cat_2',
                    'fio'     : 'Фамилия3 Имя3',
                    'status'  : 'out',
                    'early'   : true,
                    'later'   : false
                },
                'id4': {
                    'datein'  : '05.07.2017',
                    'dateout' : '07.07.2017',
                    'nights'  : '2',
                    'room'    : '202',
                    'cat'     : 'id_cat_2',
                    'fio'     : 'Фамилия4 Имя4',
                    'status'  : 'confirmed',
                    'early'   : false,
                    'later'   : true
                },
                'id5': {
                    'datein'  : '08.07.2017',
                    'dateout' : '10.07.2017',
                    'nights'  : '2',
                    'room'    : '303',
                    'cat'     : 'id_cat_3',
                    'fio'     : 'Фамилия5 Имя5',
                    'status'  : 'not-confirmed',
                    'early'   : false,
                    'later'   : false
                },
                'id6': {
                    'datein'  : '03.07.2017',
                    'dateout' : '08.07.2017',
                    'nights'  : '5',
                    'room'    : '105',
                    'cat'     : 'id_cat_1',
                    'fio'     : 'Резерв',
                    'status'  : 'reserved',
                    'early'   : false,
                    'later'   : false
                },
                'id7': {
                    'datein'  : '04.07.2017',
                    'dateout' : '09.07.2017',
                    'nights'  : '5',
                    'room'    : '205',
                    'cat'     : 'id_cat_2',
                    'fio'     : 'Заблокировано',
                    'status'  : 'blocked',
                    'early'   : false,
                    'later'   : false
                },
                'id8': {
                    'datein'  : '04.07.2017',
                    'dateout' : '09.07.2017',
                    'nights'  : '5',
                    'room'    : '101',
                    'cat'     : 'id_cat_1',
                    'fio'     : 'Фамилия6 Имя6',
                    'status'  : 'confirmed',
                    'early'   : true,
                    'later'   : true
                },
                'id9': {
                    'datein'  : '10.07.2017',
                    'dateout' : '11.07.2017',
                    'nights'  : '1',
                    'room'    : '104',
                    'cat'     : 'id_cat_1',
                    'fio'     : 'Фамилия7 Имя7',
                    'status'  : 'not-confirmed',
                    'early'   : true,
                    'later'   : true
                }
            }
            chessParseBookings(res);
        }
    });
}

//парсер полученных бронирований
function chessParseBookings(res) {
    var bookingsHTML = '';
    $.each(res, function(key,val){
        var addClass = '';
        if (val.early) {
            addClass += ' early';
        }
        if (val.later) {
            addClass += ' later';
        }

        bookingsHTML += '<a href="#" class="booking hide '+val.status+' '+addClass+'" data-id="'+key+'" data-cat="'+val.cat+'" data-room="'+val.room+'" data-nights="'+val.nights+'" data-datein="'+val.datein+'" data-dateout="'+val.dateout+'"><span class="text">'+val.fio+' <i class="wave left"></i><i class="wave right"></i></span></a>';
    });
    $chessBookings.html(bookingsHTML);

    chessRefreshPositionBookings();
    chessInitDraggable();
}

//очистка бронирований
function chessClearBookings() {
    $chessBookings.find('.booking').remove();
}

//обновление позиций бронирований по ячейкам и доступности перетаскиваний!
function chessRefreshPositionBookings($booking) {
    //очищаем ячейки под бронирования
    $chessWrapper.find('.rooms .cell').removeClass('disable current early later first').attr('data-booking', '');

    //из выборки броней исключаем клон drag элемента
    $chessBookings.children('.booking').not('.ui-draggable-dragging').each(function(){
        //устанавливаем позицию брони!
        chessSetPositionBooking($(this));
    }).removeClass('hide'); 
}

function chessSetPositionBooking($this) {
    //var $this = $(this);
    var id      = $this.attr('data-id');
    var cat     = $this.attr('data-cat');
    var room    = $this.attr('data-room');
    var datein  = $this.attr('data-datein');
    var dateout = $this.attr('data-dateout');
    var nights  = $this.attr('data-nights') * 1;
    var early   = $this.hasClass('early');
    var later   = $this.hasClass('later');

    //находим номер
    var $objRoom = $chessWrapper.find('.rooms[data-cat="'+cat+'"] .row[data-room="'+room+'"]');
    //находим ячейку с датой заезда
    var $objCell = $objRoom.children('.cell[data-date="'+datein+'"]');

    //ширина ячейки
    var width = CHESS_WIDTH_CELL * nights;

    //получаем левый/верхний отступ для бронирования
    var left;
    var top;
    top  = $objRoom.position().top;
    if ($objCell.length) {
        left = $objCell.position().left * 1;
    } else {
        //проверяем на попадание брони в период
        //берем дату выезда - если ячейка есть, значит бронь есть
        //вычисляем для неё отступы!
        var $objCellOut = $objRoom.children('.cell[data-date="'+dateout+'"]');
        if ($objCellOut.length) {
            left = $objCellOut.position().left * 1;
            left = left - width;
        }
    }

    //console.log('l='+left+'|');
    //если левый отступ задан - значит бронь есть, позиционируем её
    if (left != undefined) {
        $this.css({
            'width' : width,
            'left'  : left,
            'top'   : top
        }).attr({
            'data-left' : left,
            'data-top'  : top
        });

        //отмечаем ячейки, в которые нельзя перетаскивать элементы!
        var firstIndex = $objRoom.children('.cell').index($objCell);
        var firstCell = firstIndex;
        //если ранний заезд
        var firstClass = 'first';
        if (early) { firstClass = ' early'; }

        var lastCell = firstIndex + nights;
        //если поздний выезд
        var lastClass = '';
        if (later) { 
            lastCell = lastCell + 1;
            lastClass = 'later';
        }
        
        var $objCells = $objRoom.children('.cell').slice(firstCell, lastCell);
        $objCells.addClass('disable').attr('data-id', id);
        $objCells.first().addClass(firstClass);
        $objCells.last().addClass(lastClass);

    } else {
        //иначе удаляем
        $this.remove();
    }
}

//получение количества доступных и нераспределенным номеров по категориям!
function chessGetRooms() {
    //получаем период
    var $dates = $('#calendar_dates').children('.cell');
    var start = $dates.first().attr('data-date');
    var finish = $dates.last().attr('data-date');

    $.ajax({
        url      : '/ajax/#getrooms',
        type     : 'POST',
        dataType : 'json',
        data     : {
            'period': start+'-'+finish
        },
        timeout  : 60000,
        success: function(res){
            chessParseRooms(res);
        },
        error: function(res){
            var res = {
                'id_cat_1': { //ID категории
                    '0': { //первая дата
                        'all': 10, //всего доступных номеров
                        'new': 0 //нераспределенных/новых броней
                    },
                    '1': { //вторая дата и т.д.
                        'all': 5,
                        'new': 0
                    } 
                },
                'id_cat_2': {
                    '0': {
                        'all': 3,
                        'new': 0
                    },
                    '1': {
                        'all': 2,
                        'new': 0
                    }
                },
                'id_cat_3': {
                    '0': {
                        'all': 10,
                        'new': 0
                    },
                    '1': {
                        'all': 4,
                        'new': 0
                    }
                }
            }
            chessParseRooms(res);
        }
    });
}

//парсер полученной JSON доступности номеров по категориям!
function chessParseRooms(res) {
    var $rooms = $('#rooms_wrapper');

    //текущая дата + 5 дней
    //пока статичная JSON, потом убрать!
    var curDate = '26.06.2017';

    $.each(res, function(key,val){
        $rooms.children('.row.title[data-cat="'+key+'"]').children('.cell').each(function(i){
            var arr = val[i];
            var count_all = 0;
            var count_new = 0;
            if (arr == undefined) {
                count_all = val[0]['all'];
                count_new = 0;
                if ($(this).attr('data-date') == curDate) {
                    count_new = 3;
                }

            } else {
                count_all = arr['all'];
                count_new = arr['new'];
            }
            var countHTML = '';
            countHTML = count_all;
            //если 0 доступных выделяем!
            if (count_all == 0) {
                countHTML = '<b>'+countHTML+'</b>';
            }

            var addClass = '';
            //если есть новые/нераспределенные брони
            if (count_new > 0 && count_new != undefined) {
                addClass = 'link';
                countHTML += '<span class="new">'+count_new+'</span>';
            }

            countHTML = '<div class="count '+addClass+'" data-cat="'+key+'">'+countHTML+'</div>';

            $(this).html(countHTML);
        });
        
    });
}

//инициализация принимающих ячеек
function chessInitDroppable() {
    $chessWrapper.find('.rooms .cell').droppable({
        activeClass: 'active',
        scope: 'bookings',
        accept: '.booking',
        tolerance: 'pointer',
        drop: function(event, ui) {
            //объект ячейки в которую перемещаем
            var $this = $(this);
            //объект комнаты
            var $objRoom = $this.parent();
            var left = $this.position().left;
            var top = $this.parent().position().top;

            //id комнаты
            var room = $objRoom.attr('data-room');
            //id категории
            var cat = $objRoom.parent().attr('data-cat');

            //объект бронирования
            var $objBooking = $chessBookings.children('.booking.dragging');

            //id брони
            var id = $objBooking.attr('data-id');
            //количество ночей
            var nights = $objBooking.attr('data-nights') * 1;
            //ранний заезд
            var early = $objBooking.hasClass('early');
            //поздний выезд
            var later = $objBooking.hasClass('later');

            //убираем disable с ячеек, в которых находилось текущее бронирование
            $chessWrapper.find('.rooms .cell[data-id="'+id+'"]').removeClass('disable').addClass('current');

            //проверяем на пересечение бронирований
            //индекс ячейки для добавления
            var firstIndex = $objRoom.find('.cell').index($(this));
            var firstCell = firstIndex;
            //если ранний заезд
            //проверку начинаем с предыудщей ячейки
            //if (early) { firstCell = firstCell - 1; }

            var lastIndex = firstIndex + nights;
            var lastCell = lastIndex;
            //если поздний выезд
            //проверку завершаем с последней+1 ячейки
            if (later) { lastCell = lastCell + 1; }
            //lastCell = lastCell+1;

            //флаг проверки
            var check = 1;
            $objRoom.find('.cell').slice(firstCell, lastCell).each(function(){
                //если хотя бы 1 ячейка недоступна
                //значит получаем прошлые значения отступов и отменяем перенос
                //console.log($(this).data('date'));
                if (
                    $(this).hasClass('disable') ||
                    $(this).next().hasClass('disable early') ||
                    (early && $(this).prev().hasClass('disable') && !$(this).prev().hasClass('later')) ||
                    (later && $(this).hasClass('disable'))
                ){
                    left = $objBooking.attr('data-left');
                    top  = $objBooking.attr('data-top');
                    check = 0;

                    return false;
                }
            });

            //даты брони
            //если перетаскивание успешно
            if (check == 1) {
                //получаем новые даты заезда/выезда
                var datein  = $this.attr('data-date');
                var dateout =  $objRoom.find('.cell').eq(lastIndex).attr('data-date');
                //вызываем ф-ю формирования дальнейших действия по брони!
                chessBookingChange($objBooking, cat, room, datein, dateout, left, top);

            } else {
                //иначе перемещаем обратно бронь
                $objBooking.css({
                    'left': left,
                    'top': top
                });
            }    
        }
    });
}

//изменение параметров бронирования
//на вход объект брони и обновленные параметры: категория, номер, даты заезда/выезда, левый/верхний отступы
function chessBookingChange($booking, cat, room, datein, dateout, left, top) {
    var $modal = $('#change_category_booking_modal');

    //получаем информацию о текущих параметрах брони
    var catCurrent     = $booking.attr('data-cat');
    var roomCurrent    = $booking.attr('data-room');
    var dateinCurrent  = $booking.attr('data-datein');
    var dateoutCurrent = $booking.attr('data-dateout');
    //получаем статус брони
    var status = $booking.attr('data-status');
    //текущую позицию брони
    var leftCurrent = $booking.attr('data-left');
    var topCurrent = $booking.attr('data-top');

    //если изменилась категория номера брони
    //отправляем AJAX запрос на получение параметров переселения
    if (catCurrent != cat && !$booking.hasClass('reserved') && !$booking.hasClass('blocked')) {
        //получаем названия категорий
        var catNameCurrent = $('#chess_sidebar .cat[data-cat="'+catCurrent+'"]').attr('title');
        var catName = $('#chess_sidebar .cat[data-cat="'+cat+'"]').attr('title');

        //добавляем в HTML данные
        $modal.find('.cat-current').text(catNameCurrent);
        $modal.find('.room-current').text(roomCurrent);
        $modal.find('.date-current').text(dateinCurrent+' - '+dateoutCurrent);
        $modal.find('.cat-new').text(catName);
        $modal.find('.room-new').text(room);
        $modal.find('.date-new').text(datein+' - '+dateout);


        showChangeCategoryBookingModal();

    } else {
        //иначе пока завершаем перенос
        changeSuccess();
    }


    //показ модального окна изменения категории бронирования
    function showChangeCategoryBookingModal() {
        $modal.show('fade');
    }
    //закрытие модального окна изменения категории бронирования
    function closeChangeCategoryBookingModal() {
        $('#change_category_booking_modal').hide();
    }

    //успешное изменение брони
    function changeSuccess() {
        //обновляем параметры брони!
        $booking.css({
            'left': left,
            'top': top
        }).attr({
            'data-cat'     : cat,
            'data-room'    : room,
            'data-datein'  : datein,
            'data-dateout' : dateout,
            'data-left'    : left,
            'data-top'     : top
        });
        //обновляем позиционирование бронирований
        chessRefreshPositionBookings();
    }
    //отказ от изменения брони
    function changeCancel() {
        //иначе перемещаем обратно бронь
        $booking.css({
            'left': leftCurrent,
            'top': topCurrent
        });
    }

    //пока размещаем здесь, только для демонстрации
    $('#change_category_booking_submit').on('click', function(){
        changeSuccess();
        closeChangeCategoryBookingModal();
        return false;
    });
    //отказ от изменения
    $('#change_category_booking_cancel').on('click', function(){
        changeCancel();
        closeChangeCategoryBookingModal();
    });
}



//переинициализация принимающих ячеек
function chessReinitDroppable() {
    $chessWrapper.find('.rooms .cell').droppable('disable');
    $chessWrapper.find('.rooms .cell').filter(':visible').droppable('enable');
}

//ицициализация перетаскивания броней
function chessInitDraggable() {
    $chessBookings.children('.booking').not('.out').draggable({
        containment: '#rooms_wrapper',
        helper: 'clone',
        //snap: true,
        handle:'.text',
        scope: 'bookings',
        revert: 'invalid',
        refreshPositions: false,
        addClasses: false,
        zIndex: 200,
        distance: 5,
        cursorAt: {left: 0, top: 15},
        //grid:[CHESS_WIDTH_CELL,35],
        start: function(event, ui) {
            $(this).addClass('dragging');
        },
        stop: function(event, ui) {
            $(this).removeClass('dragging');
        },
        drag:function(event, ui){
            
        }
    });
    //клик по бронированию на шахматке
    $chessBookings.children('.booking').on('click', function(){
        var $this = $(this);
        if ($(this).hasClass('rezerved') || $(this).hasClass('booking')) {
            //swal('ok');
        }
    });
}

//добавление бронирования выделением ячеек
function chessInitSelectable() {
    $chessWrapper.find('.rooms .row').selectable({
        autoRefresh: true,
        cancel:'.cell.disable, .cell.archive',
        filter: '.cell:not(.archive, .disable:not(.first))',
        //tolerance: 'fit',
        start: function(event, ui) {
            $chessWrapper.find('.ui-selected').removeClass('ui-selected');
            $chessBookings.find('.booking.new').remove();
        },
        selecting: function(event, ui) {

        },
        stop: function(event, ui) {
            var $this = $(this);
            //получаем выделенные ячейки
            var $selected = $this.find('.ui-selected');
            var $first = $selected.first();

            //получаем последний элемент выделения, не учитываем disable
            var $last  = $selected.last();

            //ячейки номера
            var $cells = $this.find('.cell');
            //получаем индексы первой и последней выделенной ячейки
            var indexFirst = $cells.index($first);
            var indexLast = $cells.index($last);
            //получаем выбранные ячейки
            //и проверяем выбранный диапазон на недоступные даты
            var $cellsRange = $cells.slice(indexFirst, indexLast);
            if ($cellsRange.filter(':not(.ui-selected)').length) {
                $last = $cellsRange.filter(':not(.ui-selected)').first();
                //if ($last.hasClass('early')) {
                    $last = $last.prev();
                //}
            }

            //если в диапазоне одна ячейка
            //тогда для последней выбираем следующую ячейку
            var datein = $first.attr('data-date');
            var dateout = $last.attr('data-date');
            if (datein == dateout) {
                $last = $last.next();
            }
            
            /*добавляем бронирование*/
            chessAddBooking($first, $last);
        }
    });
}

//добавление брони с диалоговым окном при выборе промежутка
//на вход первый и последний выделенный объект/ячейка
function chessAddBooking($first, $last) {
    //объект номера
    var $objRoom = $first.parent();
    //объект категории
    var $objCat = $objRoom.parent();
    
    //получаем номера объектов в выборке
    var $objCells = $objRoom.find('.cell');

    var firstIndex = $objCells.index($first);
    var lastIndex = $objCells.index($last);

    //получаем параметры добавляемой брони
    var datein = $first.attr('data-date');
    var dateout = $last.attr('data-date');
    var room = $objRoom.attr('data-room');
    var cat = $objCat.attr('data-cat');
    var nights = lastIndex-firstIndex;
    var status = 'new';

    //console.log($last.attr('class'));
    //проверяем на возможность добавления брони
    if (
        ($last.hasClass('disable') && $last.hasClass('early')) || 
        $first.hasClass('disable')
    ) {
        //выводим сообщение о невозможности создания брони
        showWarning('Номер занят', 'в выбранный период с '+datein+' по '+dateout);

    } else {
        //добавляем бронь в HTML
        //получаем ширину и отступы брони
        var width = CHESS_WIDTH_CELL * nights;
        var left = $first.position().left;
        var top = $objRoom.position().top;
        var bookingHTML = '<div class="booking new" data-id="new" data-cat="'+cat+'" data-room="'+room+'" data-nights="'+nights+'" data-datein="'+datein+'" data-dateout="'+dateout+'" style="display:none; width:'+width+'px; left:'+left+'px; top:'+top+'px;"><span class="text"><i class="wave left"></i><i class="wave right"></i></span></div>';

        $chessBookings.append(bookingHTML);

        var $objNewBooking = $chessBookings.children('.new');
        $objNewBooking.show('fade', 400);
        //позиционируем и показываем диалоговое окно
        chessSetPositionDialogBooking($objNewBooking);
        chessShowDialogBooking();
    }  
}

//позиционирование диалогового окна создание брони на шахматке
//на вход объект брони для которой показываем диалог!
function chessSetPositionDialogBooking($booking) {
    //позиционирование диалогового окна
    //получаем размеры и смещение брони от вьюпорта
    var bookingWidth  = $booking.width();
    var bookingHeight = $booking.height();
    var bookingOffset = $booking.offset();
    var bookingLeft   = bookingOffset.left;
    var bookingRight  = bookingLeft + bookingWidth;
    var bookingTop    = bookingOffset.top;
    var bookingBottom = bookingTop + bookingHeight;

    //получаем размеры диалогового окна
    var $dialog      = $('#dialog_add_booking');
    var dialogWidth  = $dialog.width();
    var dialogHeight = $dialog.height();
    var dialogLeft   = 0;
    var dialogTop    = 0;

    var scrollTop = $(window).scrollTop();

    //позиция диалогового окна
    //доп класс для позиционирования окна
    var addClass = '';

    //расстояние от правого края брони до правого края вьюпорта
    var distanceRight = viewportWidth - bookingRight - 40;
    //расстояние от брони по низу вьюпорта
    var distanceBottom = viewportHeight + scrollTop - bookingBottom - 50;
    //проверяем возможность добавления диалога справа от брони!
    //если полученное расстояние больше ширины диалогового окна
    //позиционируем диалоговое окно справа
    if (distanceRight > dialogWidth) {
        dialogLeft = bookingRight + 10;
        dialogTop = bookingTop + 2;
        addClass = 'right';
    } else  {
        //если размещение справа невозможно
        //проверяем возможность размещения снизу от брони
        
        //console.log(scrollTop);
        //console.log($(window).height()+'|'+viewportHeight+'|'+bookingBottom);
        //если полученное расстояние больше ширины диалогового окна
        //позиционируем диалоговое окно снизу
        dialogLeft = bookingLeft + bookingWidth/2 - dialogWidth/2 - 15;

        if (distanceBottom > dialogHeight) {
            dialogTop = bookingTop + 40;
            addClass = 'bottom';

        } else {
            //иначе позиционируем сверху от брони
            dialogTop = bookingTop - dialogHeight - 40;
            addClass = 'top';
        }

        //проверяем умещается ли диалоговое окно по ширине справа
        //расстояние от правого края диалогового окна до правого края вьюпорта
        var distanceDialogRight = viewportWidth - dialogLeft - dialogWidth;
        //если расстояние меньше, тогда позиционируем слева
        //console.log(distanceDialogRight);
        if (distanceDialogRight < 70) {
            dialogLeft = bookingLeft - dialogWidth - 40;
            dialogTop = bookingTop + 2;
            addClass = 'left';
        }
    }
    $dialog.css({
        'left': dialogLeft,
        'top': dialogTop
    }).removeClass('right top left bottom').addClass(addClass).show('fade');
}
//закрытие диалогового окна создания брони на шахматке
function chessShowDialogBooking() {
    //добавляем пиксель
    $('body').append('<div id="fade_dialog_booking" class="fade"></div>');
    $('#dialog_add_booking').show('fade');
}
//закрытие диалогового окна создания брони на шахматке
function chessCloseDialogBooking() {
    //удаляем пиксель
    $('#fade_dialog_booking').remove();
    $('#dialog_add_booking').hide();
    //удаляем бронь и отметки с ячеек
    $chessBookings.find('.new').remove();
    $chessWrapper.find('.cell.ui-selected').removeClass('ui-selected');
}

//обновление шахматки в случае изменения периода
function chessRefresh() {
    //получаем доступность номеров
    chessGetRooms();
    //получаем брони
    chessGetBookings();
    //инициализация droppable ячеек
    chessInitDroppable();
    //инициализация selectable ячеек
    chessInitSelectable();
}

//ицициализация перетаскивания новых броней из сайдбара
function chessSidebarInitDraggable() {
    $('#new_bookings').find('.booking').draggable({
        //containment: '#rooms_wrapper',
        helper: 'clone',
        scope: 'bookings-new',
        revert: 'invalid',
        refreshPositions: false,
        addClasses: false,
        zIndex: 200,
        cursorAt: {left: 0, top: 15},
        start: function(event, ui) {
            $(this).addClass('dragging');
        },
        stop: function(event, ui) {
            $(this).removeClass('dragging');
        },
        drag:function(event, ui){
            
        }
    });
}

//инициализация принимающих ячеек новых броней из сайдбара
function chessSidebarInitDroppable() {
    $('#rooms_wrapper .sidebar-drop').droppable({
        activeClass: 'active',
        scope: 'bookings-new',
        accept: '.booking',
        tolerance: 'pointer',
        drop: function(event, ui) {
            //объект ячейки в которую перемещаем
            var $this = $(this).parent();
            //объект комнаты
            var $objRoom = $this.parent();
            var left = $this.position().left;
            var top = $this.parent().position().top;

            //id комнаты
            var room = $objRoom.attr('data-room');
            //объект категории
            var $objCat = $objRoom.parent();
            //id категории
            var cat = $objCat.attr('data-cat');

            //объект бронирования
            var $objBooking = $('#new_bookings .booking.dragging');

            //дата заезда/выезда
            var datein = $objBooking.attr('data-datein');
            var dateout = $objBooking.attr('data-dateout');

            //объект брони в сайдбаре
            var $itemBookingRight = $objBooking.parent().parent();

            //id брони
            var id = $objBooking.attr('data-id');
            //количество ночей
            var nights = $objBooking.attr('data-nights') * 1;
            //ранний заезд
            var early = $objBooking.hasClass('early');
            //поздний выезд
            var later = $objBooking.hasClass('later');

            //проверяем на пересечение бронирований
            //индекс ячейки для добавления
            var firstIndex = $objRoom.find('.cell').index($this);
            var firstCell = firstIndex;
            //если ранний заезд
            //проверку начинаем с предыудщей ячейки
            //if (early) { firstCell = firstCell - 1; }

            var lastIndex = firstIndex + nights;
            var lastCell = lastIndex;
            //если поздний выезд
            //проверку завершаем с последней+1 ячейки
            if (later) { lastCell = lastCell + 1; }
            //lastCell = lastCell+1;

            //console.log(firstCell+'|'+lastCell);

            //флаг проверки
            var check = 1;
            $objRoom.find('.cell').slice(firstCell, lastCell).each(function(){
                //console.log($())
                //если хотя бы 1 ячейка недоступна
                //значит получаем прошлые значения отступов и отменяем перенос
                if (
                    $(this).hasClass('disable') ||
                    $(this).next().hasClass('disable early') ||
                    (early && $(this).prev().hasClass('disable') && !$(this).prev().hasClass('later')) ||
                    (later && $(this).hasClass('disable'))
                ){
                    check = 0;

                    return false;
                }
            });

            //даты брони
            //если перетаскивание успешно
            if (check == 1) {
                //добавляем ID брони к номеру
                $objBooking.attr('data-room', room);
                //добавляем в основную HTML броней перетаскиваемую бронь!
                //отмечаем добавляемую бронь
                $objBooking.addClass('add');

                $chessBookings.append($objBooking);
                $chessBookings.children('.booking.add').removeAttr('style').removeClass('dragging ui-draggable-dragging ui-draggable-handle');
                $itemBookingRight.remove();

                //переинициализация draggable броней
                chessInitDraggable();
                //позиционирование добавленной брони!
                chessSetPositionBooking($('#bookings_wrapper .booking.add'));
                //снимаем отметку с добавляемое брони
                $('#bookings_wrapper .booking').removeClass('add');

                //запрос на присвоение бронированию номера!
                chessSetRoomBookings(id, room);

                //обновляем кол-во нераспреденных броней
                var $objCountCell = $chessWrapper.children('.row[data-cat="'+cat+'"]').find('.count.active');
                var $objCount = $objCountCell.children('.new');
                var newCount = $objCount.text() * 1;
                newCount--;
                if (newCount <= 0) {
                    //если нераспределенных номеров нет
                    //удаляем кол-во, убираем ячейку и закрываем сайдбар
                    $objCount.remove();
                    $objCountCell.removeClass('active link');
                    $('body').removeClass('sidebar-right-open');

                } else {
                    $objCount.text(newCount);
                }

            } else {
                showWarning('Номер занят', 'в выбранный период с '+datein+' по '+dateout);
                //alert('Распределение невозможно!');
            }
        }
    });
}

//удаление функционала drag/drop ячеек новых броней из сайдбара!
function chessSidebarDestroyDragDrop() {
    $('#new_bookings').find('.booking').draggable('destroy');
    $('#rooms_wrapper .sidebar-drop').droppable('destroy');
}

//получение нераспределенных/новых бронирований
//на вход дата ДД.ММ.ГГГГ и категория номера
function chessGetNewBookings(date, cat) {
    $('#new_bookings').html('').addClass('load');

    //console.log(date+'|'+cat);
    $.ajax({
        url      : '/ajax/',
        type     : 'POST',
        dataType : 'json',
        data     : {
            'date': date,
            'cat' : cat
        },
        timeout  : 60000,
        success: function(res){
            chessParseNewBookings(res);
        },
        error: function(res){
            var res = {
                'new_id1': {
                    'datein'  : '26.06.2017', //заезд
                    'dateout' : '29.06.2017', //выезд
                    'nights'  : '3', //ночей
                    'fio'     : 'Александ Молчановский', //фио
                    'status'  : 'confirmed', //статус бронирования
                    'cat'     : cat, //категория номера
                    'early'   : true, //ранний заезд
                    'later'   : false, //поздний выезд
                    'adults'  : 2, //кол-во взрослых
                    'childs'  : 0, //кол-во детей
                    'channel' : 'booking' //канал продажи
                },
                'new_id2': {
                    'datein'  : '26.06.2017',
                    'dateout' : '02.07.2017',
                    'nights'  : '6',
                    'fio'     : '2 Александ Молчановский',
                    'status'  : 'confirmed',
                    'cat'     : cat,
                    'early'   : true,
                    'later'   : true,
                    'adults'  : 3,
                    'childs'  : 2,
                    'channel' : 'site'
                },
                'new_id3': {
                    'datein'  : '26.06.2017',
                    'dateout' : '27.06.2017',
                    'nights'  : '1',
                    'fio'     : '3 Александ Молчановский',
                    'status'  : 'confirmed',
                    'cat'     : cat,
                    'early'   : false,
                    'later'   : false,
                    'adults'  : 2,
                    'childs'  : 1,
                    'channel' : 'site'
                }
            }
            setTimeout(function(){ chessParseNewBookings(res, cat); }, 1000);
        }
    });
}
//парсер полученной JSON нераспределенных/новых бронирований
function chessParseNewBookings(res) {
    var bookingsHTML = '';

    $.each(res, function(key, val) {
        //собираем параметры брони
        var id      = key;
        var datein  = val.datein;
        var dateout = val.dateout;
        var nights  = val.nights;
        var fio     = val.fio;
        var status  = val.status;
        var cat     = val.cat;
        var early   = val.early;
        var later   = val.later;
        var adults  = val.adults;
        var childs  = val.childs;
        var channel = val.channel;

        //период брони
        var dateinArr  = datein.split('.');
        var dateoutArr = dateout.split('.');
        var periodText = dateinArr[0]+'.'+dateinArr[1]+' - '+dateoutArr[0]+'.'+dateoutArr[1];

        //ширину брони
        var widthBooking = nights * CHESS_WIDTH_CELL;

        //допкласс
        var addClass = '';
        //статус брони
        addClass += ' '+status;
        //если ранний зеазд
        if (early) { addClass += ' early'; }
        //если поздний выезд
        if (later) { addClass += ' later'; }


        bookingsHTML += '<div class="item"><div class="info"><i class="icon icon-calendar"></i><span class="text">'+periodText+'</span><i class="icon icon-male"></i><span class="text">x'+adults+'</span><i class="icon icon-child"></i><span class="text">x'+childs+'</span><i class="ico channel-'+channel+'"></i></div><div class="booking-wrap"><div class="booking '+addClass+'" data-id="'+key+'" data-cat="'+cat+'" data-room="" data-nights="'+nights+'" data-datein="'+datein+'" data-dateout="'+dateout+'" style="width: '+widthBooking+'px; "><span class="text">'+fio+' <i class="wave left"></i><i class="wave right"></i></span></div></div></div>';
    });
    //добавляем сформированный HTML на страницу
    $('#new_bookings').removeClass('load').html(bookingsHTML);

    //инициализация drag броней
    chessSidebarInitDraggable();
}

//отправка запроса на присвоение бронированию номера
function chessSetRoomBookings(id, room) {
    //console.log(id+'|'+room);
    $.ajax({
        url      : '/ajax/',
        type     : 'POST',
        dataType : 'json',
        data     : {
            'id': id,
            'room' : room
        },
        timeout  : 60000,
        success: function(res){
        },
        error: function(res){
        }
    });
}

//формирование/показ модального окна добавления/редактирования резерва/блокировки
//на вход объект брони и status - reserved/blocked
function chessShowModalReserveBlock($booking, status) {
    var $modal = $('#reserve_block_modal');
    //собираем данные
    var id      = $booking.attr('data-id');
    //var cat     = $booking.attr('data-cat');
    var room    = $booking.attr('data-room');
    var datein  = $booking.attr('data-datein');
    var dateout = $booking.attr('data-dateout');
    //var nights  = $booking.attr('data-nights') * 1;
    //var early   = $booking.hasClass('early');
    //var later   = $booking.hasClass('later');
    //console.log($booking.attr('data-datein'));
    var caption = '';
    if (status == 'reserved') {
        caption = 'Резерв';
    } else {
        caption = 'Заблокировать';
    }

    $booking.attr('data-status', status);

    //добавляем данные в формы
    $modal.find('.caption').text(caption);
    $modal.find('input[name="datein"]').val(datein);
    $modal.find('input[name="dateout"]').val(dateout);
    $modal.find('input[name="room"]').val(room);
    $modal.find('input[name="id"]').val(id);
    $modal.find('input[name="status"]').val(status);

    $modal.show('fade');
}

//закрытие модального окна добавления/редактирования резерва/блокировки
function chessCloseModalReserveBlock($booking) {
    var $modal = $('#reserve_block_modal');
    $modal.hide();
    //закрываем диалоговое окно
    chessCloseDialogBooking();
    //очищаем форму
    clearForm($('#reserve_block_form'));
    $('#reserve_block_form').removeClass('load');
}

//отправка AJAX на добавление резерва/блокировки
//на вход JSON с добавляемыми данными
function chessSendAjaxAddBooking(data) {
    $.ajax({
        url      : '/ajax/',
        type     : 'POST',
        dataType : 'json',
        data     : {
            'data': data
        },
        timeout  : 60000,
        success: function(res){
            
        },
        error: function(res){
            var date = new Date();
            var id = 'id'+date.getMilliseconds();
            //в ответ жду ID добавленное брони!
            var res = {
                'id' : id
            }
            chessParseAjaxAddBooking(res);
        }
    });
}
//парсинг JSON на добавление резерва/блокировки
function chessParseAjaxAddBooking(res) {
    if (res.id != undefined) {
        //alert(res.id);
        var $booking = $chessBookings.children('.booking.new');
        var $form = $('#reserve_block_form');
        var name = $form.find('textarea[name="name"]').val();

        var status = $booking.attr('data-status');
        $booking.removeClass('new').addClass(status).children('.text').text(name);
        chessCloseDialogBooking();
        chessCloseModalReserveBlock();
        chessInitDraggable();
    } else {
        alert('ОШИБКА!!!');
    }
}


//тарифы

//получение стоимости по категориям!
//на вход ID тарифа, иначе будет вывод всех тарифов!
function ratesGetPrices(tarif) {
    $('#chess_content').addClass('load');
    //получаем период
    var $dates = $('#calendar_dates').children('.cell');
    var start = $dates.first().attr('data-date');
    var finish = $dates.last().attr('data-date');

    if (tarif == undefined) {
        tarif = '';
    }

    $.ajax({
        url      : '/ajax/#ratesGetPrices',
        type     : 'POST',
        dataType : 'json',
        data     : {
            'tarif': tarif,
            'period': start+'-'+finish
        },
        timeout  : 60000,
        success: function(res){
            ratesParsePrices(res);
        },
        error: function(res){
            var res = {
                'rates': { //цены по тарифам
                    'id_cat_1': { //ID категории
                        'id_rate_1': { //тариф
                            '0': '5 000', //первая дата
                            '1': '5 000'  //вторая дата
                        },
                        'id_rate_2': { //тариф
                            '0': '4 000', //первая дата
                            '1': '4 000'  //вторая дата
                        } 
                    },
                    'id_cat_2': {
                        'id_rate_3': {
                            '0': '10 000',
                            '1': '10 000'
                        }
                    },
                    'id_cat_3': {
                        'id_rate_4': {
                            '0': '15 000',
                            '1': '15 000'
                        },
                        'id_rate_5': {
                            '0': '20 000',
                            '1': '20 000'
                        }
                    }
                },
                'options': { //опции тарифа
                    'id_option_1': { //опция
                        '0': '1 000', //первая дата
                        '1': '1 000'  //вторая дата
                    },
                    'id_option_2': { //опция
                        '0': '500', //первая дата
                        '1': '500'  //вторая дата
                    },
                    'id_option_3': {
                        '0': '1 500',
                        '1': '1 500'
                    }
                }
            }
            setTimeout(function(){ ratesParsePrices(res); }, 1000);
        }
    });
}

//парсер полученной JSON стоимости номеров!
function ratesParsePrices(res) {
    var $rates = $('#rooms_wrapper').children('.rates');

    //формируем цены по тарфам
    $.each(res['rates'], function(key,val){
        //получаем объект категории номеров
        var $cat = $rates.filter('[data-cat="'+key+'"]');
        //console.log($cat.length);
        $.each(val, function(key2, val2) {
            //получаем тариф
            var $room = $cat.children('.row[data-rate="'+key2+'"]');
            //добавляем стоимости номеров по дням
            $room.children('.cell').each(function(i){
                var price = val2[i];
                if (price == undefined) {
                    price = val2[0];
                }
                $(this).html('<p class="price">'+price+' &#8381;</p>');
            });
        });
    });

    //console.log(res['options']);

    //формируем цены по опциям
    $.each(res['options'], function(key,val){
        //получаем объект категории номеров
        var $options = $('#rates_options');

        var $option = $options.find('.row[data-option="'+key+'"]');

        $.each(val, function(key2, val2) {
            //добавляем стоимости опций по дням
            $option.children('.cell').each(function(i){
                var price = val2;
                if (price == undefined) {
                    price = val2;
                }
                console.log(price);
                $(this).html('<p class="price">'+price+' &#8381;</p>');
            });
        });
    });

    $('#chess_content').removeClass('load');
}

//очистка шахматки тарифов
function ratesClear() {
    $('#rooms_wrapper .row.title .cell').html('');
    $('#rooms_wrapper .rooms .row .cell').html('<p class="price">&mdash;</p>');
}

//получение количества доступных и нераспределенным номеров по категориям!
function ratesGetRooms() {
    //получаем период
    var $dates = $('#calendar_dates').children('.cell');
    var start = $dates.first().attr('data-date');
    var finish = $dates.last().attr('data-date');

    $.ajax({
        url      : '/ajax/#getratesrooms',
        type     : 'POST',
        dataType : 'json',
        data     : {
            'period': start+'-'+finish
        },
        timeout  : 60000,
        success: function(res){
            ratesParseRooms(res);
        },
        error: function(res){
            var res = {
                'id_cat_1': { //ID категории
                    '0': { //первая дата
                        'all': 10, //всего номеров
                        'free': 5 //свободных номеров
                    },
                    '1': { //вторая дата и т.д.
                        'all': 10,
                        'free': 2
                    } 
                },
                'id_cat_2': {
                    '0': {
                        'all': 7,
                        'free': 3
                    },
                    '1': {
                        'all': 7,
                        'free': 4
                    }
                },
                'id_cat_3': {
                    '0': {
                        'all': 2,
                        'free': 2
                    },
                    '1': {
                        'all': 2,
                        'free': 2
                    }
                }
            }
            ratesParseRooms(res);
        }
    });
}

//парсер полученной JSON доступности номеров по категориям!
function ratesParseRooms(res) {
    var $rooms = $('#rooms_wrapper');

    $.each(res, function(key,val){
        $rooms.children('.row.title[data-cat="'+key+'"]').children('.cell').each(function(i){
            var arr = val[i];
            var count_all = 0;
            var count_free = 0;
            if (arr == undefined) {
                count_all  = val[0]['all'];
                count_free = val[0]['free'];
                
            } else {
                count_all  = arr['all'];
                count_free = arr['free'];
            }

            //процент заполненности номера
            var percent = 100 - Math.round((count_free / count_all) * 100);

            $(this).html('<div class="count" data-cat="'+key+'"><b>'+count_free+'('+count_all+'), '+percent+'%</b></div>');
        });
        
    });
}

//обновление тарифов в случае измененеия периода
function ratesRefresh() {
    //очищаем шахматку тарифов
    ratesClear();
    //получаем информацию по заполненности номеров
    ratesGetRooms();
    //получаем информацию по стоимости номеров
    ratesGetPrices();
}

//вывод модального окна предупреждения
//на вход заголовок и сообщение
function showWarning(title, text) {
     swal({
        'title': title,   
        'text': text,
        'type': 'warning',   
        'confirmButtonColor': '#458ab6',   
        'confirmButtonText': 'Ок'
    });
}