<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\helpers\C_Role as Role;

use app\helpers\H_Rbac;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

    <!-- HTML5 Shiv and Respond.js IE8 support -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php $this->beginBody() ?>


<header class="header">
    <a href="new-booking.html" class="add-booking waves-effect waves-light">+ Создать бронирование</a>
    <a href="#" class="button waves-effect waves-light">
        <i class="icon icon-housing"></i>
    </a>
    <a href="#" class="button waves-effect waves-light">
        <i class="icon icon-mail"></i>
        <span class="badge orange">5</span>
    </a>
    <a href="#" class="button waves-effect waves-light">
        <i class="icon icon-edit-1"></i>
        <span class="badge red">3</span>
    </a>
    <a href="#" class="button waves-effect waves-light">
        <i class="icon icon-tasks"></i>
        <span class="badge green">2</span>
    </a>

    <div class="right">
        <form action="#" method="GET" class="search-form">
            <input type="text" class="input-text" placeholder="Поиск...">
            <button class="submit"><i class="icon icon-search"></i></button>
        </form>
        <a href="#" class="button waves-effect waves-light">
            <i class="icon icon-settings"></i>
        </a>
        <div class="user">

            <?= Yii::$app->user->identity->username; ?> [role = "<?= H_Rbac::getCurrentRoleName();  ?>"]

            <a href="/user/logout" class="logout waves-effect waves-light" data-method="post">
                <i class="icon icon-logout"></i>
            </a>



        </div>
    </div>
    <div class="clr"></div>
</header>

<div class="sidebar">
    <div class="hotel">
        <div class="logo" style="background-image: url(<?= \Yii::getAlias('@web'); ?>/images/maple-icon.png);"></div>
        <span class="name">
            <span class="table"><span class="cell">Название отеля</span></span>
        </span>
    </div>

    <div class="menu">
        <a id="sidebar_toggle" class="sidebar-toggle" href="#">
            <i class="icon icon-open"></i>
        </a>



            <div class="menu">


                <?php
                    echo Nav::widget([
                        'options' => [
                            'class' => 'nav in',
                            'id' => 'side-menu'
                        ],

                        'items' => [

                            // Common menu
                            ['label' => '<i class="icon icon-main"></i> Главная', 'url' => ['/']],


                            // For SuperAdmin
                            [
                                'label' => '<i class="icon icon-main"></i> Журнал действий',
                                'url' => ['/logs'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['superadmin']] )
                            ],
                            [
                                'label' => '<i class="icon icon-tabl"></i> Пользователи и Доступы',
                                'url' => ['/user/admin'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['superadmin']] )
                            ],



                            [
                                'label' => '<i class="icon icon-tabl"></i> Шахматка',
                                'url' => ['/calendar'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],
                            [
                                'label' => '<i class="icon icon-reservation"></i> Бронирования',
                                'url' => ['/bookings'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],
                            [
                                'label' => '<i class="icon icon-guests"></i> Гости',
                                'url' => ['/guests'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],
                            [
                                'label' => '<i class="icon icon-tasks"></i> Задачи',
                                'url' => ['/tasks'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],
                            [
                                'label' => '<i class="icon icon-rates"></i> Тарифы',
                                'url' => ['/rates'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],
                            [
                                'label' => '<i class="icon icon-channels"></i> Каналы',
                                'url' => ['/channels'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],
                            [
                                'label' => '<i class="icon icon-products"></i> Продажи',
                                'url' => ['/sales'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],
                            [
                                'label' => '<i class="icon icon-report"></i> Отчеты',
                                'url' => ['/reports'],
                                'visible' => Yii::$app->rbac->checkRole([\Yii::$app->params['roles']['admin']] ),
                            ],


                            ['label' => '<i class="icon icon-tabl"></i> Профиль', 'url' => ['/user/settings/profile'], 'visible' => !Yii::$app->user->isGuest],

                            // Manager menu
                            [
                                'label' => '<i class="icon icon-tabl"></i> Отели',
                                'url' => ['/hotels'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['manager'], \Yii::$app->params['roles']['admin']] )
                            ],

                            [
                                'label' => '<i class="icon icon-tabl"></i> Типы питания',
                                'url' => ['/foodtype'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['manager'], \Yii::$app->params['roles']['admin']] )
                            ],
                            [
                                'label' => '<i class="icon icon-tabl"></i> Типы номеров',
                                'url' => ['/roomtype'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['manager'], \Yii::$app->params['roles']['admin']] )
                            ],
                            [
                                'label' => '<i class="icon icon-tabl"></i> Параметры номеров',
                                'url' => ['/roomoptions'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['manager'], \Yii::$app->params['roles']['admin']] )
                            ],
                            [
                                'label' => '<i class="icon icon-tabl"></i> Типы кроватей',
                                'url' => ['/bedtype'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['manager'], \Yii::$app->params['roles']['admin']] )
                            ],
                            [
                                'label' => '<i class="icon icon-tabl"></i> Типы ванной',
                                'url' => ['/bathroomtype'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['manager'], \Yii::$app->params['roles']['admin']] )
                            ],
                            [
                                'label' => '<i class="icon icon-tabl"></i> Виды из номера',
                                'url' => ['/viewroomtype'],
                                'visible' => Yii::$app->rbac->checkRole( [\Yii::$app->params['roles']['manager'], \Yii::$app->params['roles']['admin']] )
                            ],


                        ],
                        'encodeLabels' => false,
                    ]);
                ?>
            </div>



    </div>
</div>


<div class="sidebar-right">
    <div class="caption">
        <a href="#" id="close_sidebar_right_chess" class="close"></a>
        <p id="sidebar_right_date" class="date">Вт, 30</p>
        <p id="sidebar_right_category">Полулюкс</p>
    </div>
    <div id="new_bookings" class="new-bookings">
        <!--<div class="item">
            <div class="info">
                <i class="ico ico-date"></i>
                <span class="text">10.03 - 15.03</span>
                <i class="ico ico-adults"></i>
                <span class="text">x3</span>
                <i class="ico ico-childs"></i>
                <span class="text">x1</span>
                <i class="ico channel-booking"></i>
            </div>
            <div class="booking-wrap">
                <div class="booking in early later" data-id="id1" data-cat="" data-room="" data-nights="1" data-datein="14.06.2017" data-dateout="15.06.2017" style="width: 600px; ">
                    <span class="text">Малчановский Александр <i class="wave left"></i><i class="wave right"></i></span>
                </div>
            </div>
        </div>-->
    </div>
</div>

<div id="body">
    <div class="wrapper">
<!--        <h1>--><?//= $this->title; ?><!--</h1>-->
        <?= $content ?>
    </div>
</div>


<!--диалоговое окно при добавлении брони-->
<div class="dialog right" id="dialog_add_booking">
    <div class="dialog-content">
        <ul>
            <li><a href="chess.html" class="link"><i class="icon icon-edit"></i><span class="text">Новое прямое бронирование</span></a></li>
            <li><a href="chess.html" class="link"><i class="icon icon-export"></i><span class="text">Новое внешнее бронирование</span></a></li>
            <li><a id="booking_block" class="link" href="#"><i class="icon icon-lock"></i><span class="text">Заблокировать</span></a></li>
            <li><a id="booking_reserve" class="link" href="#"><i class="icon icon-tag"></i><span class="text">Резерв</span></a></li>
        </ul>
    </div>
</div>

<!--модальное окно добавления/редактирования резерва/блокировки-->
<div class="modal" id="reserve_block_modal">
    <div class="modal-content small">
        <p class="caption">Резерв</p>
        <form id="reserve_block_form" class="form" method="POST">
            <div class="period">
                <div class="field datein">
                    <input type="text" name="datein" class="form-control validate" data-validate="date" readonly="readonly" value="10.07.2017">
                </div>
                <div class="field dateout">
                    <input type="text" name="dateout" class="form-control validate" data-validate="date" readonly="readonly" value="12.07.2017">
                </div>
                <div class="divide"></div>
                <div class="clr"></div>
            </div>
            <div class="field w100 mt-0 mb-0">
                <textarea name="name" class="form-control textarea validate" data-validate="required" placeholder="Причина"></textarea>
            </div>
            <div style="display: none;">
                <input type="hidden" name="room" value="">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="status" value="">
            </div>
            <div class="buttons align-right">
                <button class="button-form ok" id="reserve_block_button_submit">Ок</button>
                <button class="button-form cancel" id="reserve_block_button_cancel">Отмена</button>
                <button class="button-form remove disable" id="reserve_block_button_remove">Удалить</button>
            </div>
        </form>
    </div>
</div>

<!--модальное окно изменения категории брони-->
<div class="modal" id="change_category_booking_modal">
    <div class="modal-content">
        <div class="loadable-content">
            <div class="alert alert-info">
                <b>Внимание! Вы меняете категорию размещения гостя</b> <br>
                <span class="fio">Молчановский Александр</span>, #<span class="id">4564546</span>
            </div>

            <div class="modal-table">
                <table>
                    <tr>
                        <th>Категория</th>
                        <th>Номер</th>
                        <th>Даты проживания</th>
                        <th>Цена</th>
                    </tr>
                    <tr>
                        <td><span class="cat-current">НОМЕР DOUBLE (двухместный номер с 1 кроватью)</span></td>
                        <td><span class="room-current">102</span></td>
                        <td><nobr class="date-current">01.06.2017-01.07.2017</nobr></td>
                        <td><span class="sum nobr">12 000 <span class="rub">₽</span></span></td>
                    </tr>
                    <tr>
                        <td><span class="cat-new">Полулюкс</span></td>
                        <td><span class="room-new">203</span></td>
                        <td><nobr class="date-new">01.06.2017-01.07.2017</nobr></td>
                        <td><span class="sum nobr">+ 5 000 <span class="rub">₽</span></span></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-right"><span class="total">Итоговая цена</span></td>
                        <td><span class="sum big nobr">17 000 <span class="rub">₽</span></span></td>
                    </tr>
                </table>
            </div>
            <div class="buttons">
                <div class="buttons align-right">
                    <button class="button-form ok" id="change_category_booking_submit">Ок</button>
                    <button class="button-form cancel" id="change_category_booking_cancel">Отмена</button>
                </div>
            </div>
        </div>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>