<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Справочник типов номеров');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Room Type'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'hotel_id.name',
                'label' => Yii::t('app', 'Отель'),
                'format' => 'text',
                'value'=> function ($model){
                   return $model->hotel->name;
               }
            ],



            'name',
            'square',

            [
                'attribute' => 'bed_type_id.name',
                'label' => Yii::t('app', 'Тип кровати'),
                'format' => 'text',
                'value'=> function ($model){
                    return $model->bedType->name;
                }
            ],


            [
                'attribute' => 'bathroom_type_id.name',
                'label' => Yii::t('app', 'Тип ванной'),
                'format' => 'text',
                'value'=> function ($model){
                    return $model->bathroomType->name;
                }
            ],



            [
                'attribute' => 'view_room_type_id.name',
                'label' => Yii::t('app', 'Вид из номера'),
                'format' => 'text',
                'value'=> function ($model){
                    return $model->viewRoomType->name;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
