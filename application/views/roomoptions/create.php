<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RoomOptions */

$this->title = Yii::t('app', 'Create Room Options');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Room Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-options-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
