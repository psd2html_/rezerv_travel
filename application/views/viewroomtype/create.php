<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ViewRoomType */

$this->title = Yii::t('app', 'Create View Room Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'View Room Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-room-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
