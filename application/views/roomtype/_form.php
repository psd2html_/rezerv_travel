<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\RoomType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $hotels = \app\models\Hotels::find()->all();
        $items = ArrayHelper::map($hotels, 'id', 'name');
        $params = [
            'prompt' => 'Укажите отель'
        ];
        echo $form->field($model, 'hotel_id')->dropDownList($items, $params);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'square')->textInput() ?>


    <?php
        $bed_type = \app\models\BedType::find()->all();
        $items = ArrayHelper::map($bed_type, 'id', 'name');
        $params = [
            'prompt' => 'Укажите тип кровати'
        ];
        echo $form->field($model, 'bed_type_id')->dropDownList($items, $params);
    ?>



    <?php
        $bathroom_type = \app\models\BathroomType::find()->all();
        $items = ArrayHelper::map($bathroom_type, 'id', 'name');
        $params = [
            'prompt' => 'Укажите тип ванной'
        ];
        echo $form->field($model, 'bathroom_type_id')->dropDownList($items, $params);
    ?>


    <?php
        $view_room_type = \app\models\ViewRoomType::find()->all();
        $items = ArrayHelper::map($view_room_type, 'id', 'name');
        $params = [
            'prompt' => 'Укажите вид из номера'
        ];
        echo $form->field($model, 'view_room_type_id')->dropDownList($items, $params);
    ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
