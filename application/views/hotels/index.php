<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отели';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotels-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Hotels', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',


            [
                'attribute' => 'type',
                'label' => Yii::t('app', 'Тип'),
                'format' => 'text',
                'value'=> function ($data){
                    return \app\models\Hotels::itemAlias("Type", $data->type);
                }
            ],



            'stars',
            'address',
            'contact_phone',
            'mobile_phone',

            [
                'attribute' => 'sms_send',
                'label' => Yii::t('app', 'Статус SMS'),
                'format' => 'text',
                'value'=> function ($data){
                    return \app\models\Hotels::itemAlias("StatusSmsSend", $data->sms_send);
                }
            ],

            'fax',

            [
                'attribute' => 'fax_send',
                'label' => Yii::t('app', 'Статус FAX'),
                'format' => 'text',
                'value'=> function ($data){
                    return \app\models\Hotels::itemAlias("StatusFaxSend", $data->fax_send);
                }
            ],

            'contact_email:email',


            //'notify_email:email',

            [
                'attribute' => 'notify_email',
                'label' => Yii::t('app', 'Статус Email'),
                'format' => 'text',
                'value'=> function ($data){
                    return \app\models\Hotels::itemAlias("StatusNotifyEmail", $data->notify_email);
                }
            ],


            'text:ntext',

            [
                'attribute' => 'image',
                'format' => 'raw',
                'label' => Yii::t('app', 'Картинка'),
                'value'=> function ($data){
                    if (!empty($data->image))
                        return \app\models\Hotels::getImageThumbs($data->id, $data->image);
                }
            ],





            [
                'attribute' => 'logo',
                'format' => 'raw',
                'label' => Yii::t('app', 'Лого'),
                'value'=> function ($data){
                    if (!empty($data->logo))
                        return \app\models\Hotels::getImageThumbs($data->id, $data->logo);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
