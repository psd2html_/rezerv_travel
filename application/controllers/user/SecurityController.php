<?php

namespace app\controllers\user;

use dektrium\user\controllers\SecurityController as BaseAdminController;
use app\helpers\H_Logs;

class SecurityController extends BaseAdminController
{
    public $layout = '@app/views/yii2-user-module/login_register_layout';

    public function behaviors()
    {
        return [
        ];
    }
}