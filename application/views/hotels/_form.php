<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Hotels */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hotels-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php
        $items = \app\models\Hotels::itemAlias('Type');
        $params = [ 'empty' => 'Выберите тип'];
        echo $form->field($model, 'type')->dropDownList($items, $params);
    ?>


    <?= $form->field($model, 'stars')->textInput() ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => true]) ?>



    <?php
        $items = \app\models\Hotels::itemAlias('StatusSmsSend');
        $params = [ 'empty' => 'Выберите тип'];
        echo $form->field($model, 'sms_send')->dropDownList($items, $params);
    ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

    <?php
        $items = \app\models\Hotels::itemAlias('StatusFaxSend');
        $params = [ 'empty' => 'Выберите тип'];
        echo $form->field($model, 'fax_send')->dropDownList($items, $params);
    ?>

    <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>

    <?php
        $items = \app\models\Hotels::itemAlias('StatusNotifyEmail');
        $params = [ 'empty' => 'Выберите тип'];
        echo $form->field($model, 'notify_email')->dropDownList($items, $params);
    ?>


    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>


    <?php
        $dir = Yii::getAlias('@images'). '/hotels/' . $model->id . '/';
        if (!empty($model->image) && file_exists($dir . $model->image))
        {
            echo Html::img('/'.$dir . $model->image);
            echo $form->field($model, 'del_img')->checkBox(['class'=>'span-1']);
        }
    ?>
    <?= $form->field($model, 'file')->fileInput() ?>


    <?php
        $dir = Yii::getAlias('@images'). '/hotels/' . $model->id . '/';
        if (!empty($model->logo) && file_exists($dir . $model->logo))
        {
            echo Html::img('/'.$dir . $model->logo);
            echo $form->field($model, 'del_img_logo')->checkBox(['class'=>'span-1']);
        }
    ?>
    <?= $form->field($model, 'file_logo')->fileInput() ?>







    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
