<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bed_type".
 *
 * @property string $id
 * @property string $name
 * @property integer $sort
 *
 * @property RoomType[] $roomTypes
 */
class BedType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bed_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'sort' => Yii::t('app', 'параметр сортировки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomTypes()
    {
        return $this->hasMany(RoomType::className(), ['bed_type_id' => 'id']);
    }
}
