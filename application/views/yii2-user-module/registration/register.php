<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $model
 * @var dektrium\user\Module $module
 */
$this->title = Yii::t('user', 'Sign up');
?>


<div class="modal auth">
    <div class="modal-content medium">
        <p class="caption align-center"><?= Html::encode($this->title) ?></p>

        <?php
            $form = ActiveForm::begin([
                'id' => 'registration-form',

//                'fieldConfig' => [
//                    'options' => [
//                        'tag' => false,
//                    ],
//                ],
                'options' => [
                    'class' => 'form mt-15',
                ],
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
            ]);
        ?>




                <label class="field w100">
                    <?=
                        $form->field(
                            $model,
                            'username',
                            [
                                'inputOptions' => [
                                    'autofocus'     => 'autofocus',
                                    'placeholder'   => Yii::t('app', 'Name'),
                                    'class'         => 'form-control validate',
                                    'data-validate' => 'required',
                                    'tabindex'      => '1',
                                ],
                                'options' => [
                                    'tag' => null, // Don't wrap with "form-group" div
                                ],
                                'template' => '{input}{error}{hint}',
                            ]
                        )->label(false);
                    ?>
                </label>

                <label class="field w100">
                    <?=
                        $form->field(
                            $model,
                            'email',
                            [
                                'inputOptions' => [
                                    'placeholder'   => 'Email',
                                    'class'         => 'form-control validate',
                                    'data-validate' => 'required',
                                    'tabindex'      => '2',
                                ],
                                'options' => [
                                    'tag' => null, // Don't wrap with "form-group" div
                                ],
                                'template' => '{input}{error}{hint}',
                            ]
                        )->label(false);
                    ?>
                </label>

                <?php if ($module->enableGeneratingPassword == false): ?>
                    <label class="field w100">
                            <?=
                                $form->field(
                                    $model,
                                    'password',
                                    [
                                        'inputOptions' => [
                                            'placeholder'   => Yii::t('app', 'Password'),
                                            'class'         => 'form-control validate',
                                            'data-validate' => 'required',
                                            'tabindex'      => '3',
                                        ],
                                        'options' => [
                                            'tag' => null, // Don't wrap with "form-group" div
                                        ],
                                        'template' => '{input}{error}{hint}',
                                    ]
                                )->passwordInput()->label(false);
                            ?>
                    </label>
                <?php endif ?>


                <div class="buttons mt-30">
                    <?= Html::submitButton(Yii::t('user', 'Sign Up'), ['class' => 'form-control button-form w100 ok']) ?>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12 text-center">
                        <p><?= Yii::t('user', 'Have account already? Please go to'); ?>
                        <?=
                            Html::a(
                                '<span class="text">'
                                .Yii::t('user', 'Log In')
                                .'</span>',
                                ['/login'],
                                ['class' => 'text-primary m-l-5 link']
                            );
                        ?>
                        </p>
                    </div>
                </div>

        <?php ActiveForm::end(); ?>


    </div>
</div>
