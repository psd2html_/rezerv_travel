<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */
?>

<?php $form = ActiveForm::begin([
    'id' => 'auth_form',
    'options' => [
        'class' => 'form mt-15',
    ],
//    'fieldConfig' => [
//        'options' => [
//            'tag' => false,
//        ],
//    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
]); ?>


<!--модальное окно авторизации-->
<div class="modal auth">
    <div class="modal-content medium">
        <p class="caption align-center"><?= Yii::t('user', 'Forgot password'); ?></p>
        <form id="auth_form" class="form mt-15" method="GET" action="">
            <label class="field w100">
                <?=
                    $form->field($model, 'email',
                        [
                            'inputOptions' => ['placeholder'=>'Email', 'class' => 'form-control validate', 'data-validate' => 'email'],
                        ]
                    )->textInput(['autofocus' => true])->label(false);
                ?>
            </label>
            <p><?= Yii::t('user', 'Forgot instructions'); ?></p>
            <div class="buttons mt-0">
                <?=
                    Html::submitButton(Yii::t('user', 'Reset'), ['class' => 'button-form w100 ok', 'id' => 'auth_submit']);
                ?>
            </div>
        </form>
    </div>
</div>

<?php ActiveForm::end(); ?>