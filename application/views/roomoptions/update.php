<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RoomOptions */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Room Options',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Room Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="room-options-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
