<?php

namespace app\helpers;

use Yii;
use yii\rbac;

class H_Rbac
{
    /**
     * Check role
     *
     * @param $role_name - array roles
     * @return bool
     */
    public function checkRole($roles = array())
    {
        $_role_name = '';
        $role = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->identity->id);

        foreach ($role as $value) {
            $_role_name = $value->name;
        }
        if ( in_array($_role_name, $roles)) {
            return true;
        }

        return false;
    }

    /**
     * get current role name
     *
     * @return string
     */
    public static function getCurrentRoleName()
    {
        $_role_name = '';
        if (! \Yii::$app->user->isGuest)
        {
            $role = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->identity->id);
            foreach ($role as $value) {
                $_role_name = $value->name;
            }
            return $_role_name;
        }
        else
            return 'Guest';

    }
}