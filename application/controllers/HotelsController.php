<?php

namespace app\controllers;

use Imagine\Image\Box;
use Yii;
use app\models\Hotels;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\H_Logs;

use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * HotelsController implements the CRUD actions for Hotels model.
 */
class HotelsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hotels models.
     * @return mixed
     */
    public function actionIndex()
    {
        // logging
        H_Logs::AddLog('Список отелей');

        $dataProvider = new ActiveDataProvider([
            'query' => Hotels::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hotels model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        // logging
        H_Logs::AddLog('Обзор отеля');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hotels model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Hotels();

        if ($model->load(Yii::$app->request->post()))
        {
            // logging
            H_Logs::AddLog('Создание отеля');

            $model->save();

            // Attach image
            $this->attachImage($model, 'file');
            $this->attachImage($model, 'file_logo');

            return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Hotels model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
        {
            // logging
            H_Logs::AddLog('Редактирование отеля');


            //Если отмечен чекбокс «удалить файл»
            if ($model->del_img or $model->del_img_logo)
            {
               $this->deleteAttach($model, $id);
            }

            // Attach image
            $this->attachImage($model, 'file');
            $this->attachImage($model, 'file_logo');

            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Hotels model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // logging
        H_Logs::AddLog('Удаление отеля');

        // удаляем папку с картинками
        $dir = Yii::getAlias('@images' .'/hotels/'. $id);
        $this->removeDirectory($dir);

        return $this->redirect(['index']);
    }


    /**
     * удаляем папку с картинками отеля
     *
     * @param $dir
     */
    public function removeDirectory($dir)
    {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }


    /**
     * Finds the Hotels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Hotels the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hotels::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function deleteAttach($model, $id)
    {
        $thumb_w = Yii::$app->params['thumbnails']['width'];
        $thumb_h = Yii::$app->params['thumbnails']['height'];

        if ($model->del_img)
        {
            $current_image = $model->image;

            // Удаляем оригинальную картинку
            $dir = Yii::getAlias('@images' .'/hotels/'. $id . '/');
            if (file_exists( $dir . $current_image))
            {
                //удаляем файл
                unlink($dir . $current_image);
                $model->image = '';
            }
            // удаляем thumbnail
            $dir = Yii::getAlias('@images' .'/hotels/'. $id . '/thumbs/');
            if (file_exists( $dir . $thumb_w .'_'. $thumb_h . '_' . $current_image))
            {
                //удаляем файл
                unlink($dir . $thumb_w .'_'. $thumb_h . '_' . $current_image);
                $model->image = '';
            }
        }

        if ($model->del_img_logo)
        {
            $current_image = $model->logo;

            // Удаляем оригинальную картинку
            $dir = Yii::getAlias('@images' .'/hotels/'. $id . '/');
            if (file_exists( $dir . $current_image))
            {
                //удаляем файл
                unlink($dir . $current_image);
                $model->logo = '';
            }
            // удаляем thumbnail
            $dir = Yii::getAlias('@images' .'/hotels/'. $id . '/thumbs/');
            if (file_exists( $dir . $thumb_w .'_'. $thumb_h . '_' . $current_image))
            {
                //удаляем файл
                unlink($dir . $thumb_w .'_'. $thumb_h . '_' . $current_image);
                $model->logo = '';
            }
        }
    }


    public function attachImage($model, $field)
    {
        $file = UploadedFile::getInstance($model, $field);
        if ($file && $file->tempName)
        {
            $model->$field = $file;
            if ($model->validate([$field]))
            {
                $thumb_w = Yii::$app->params['thumbnails']['width'];
                $thumb_h = Yii::$app->params['thumbnails']['height'];

                $dir = Yii::getAlias('@images'). '/hotels/' . $model->id . '/';

                // create directory
                Yii::$app->controller->createDirectory($dir);

                //$dir .= substr(md5(microtime()), mt_rand(0, 30), 2) . '/' . substr(md5(microtime()), mt_rand(0, 30), 2) . '/';
                $fileName = substr(md5($model->$field->baseName), 0, 15) . '.' . $model->$field->extension;
                $fileNameThumbs = $thumb_w .'_'. $thumb_h . '_' . substr(md5($model->$field->baseName), 0, 15) .'.'. $model->$field->extension;

                $model->$field->saveAs($dir . $fileName);
                $model->$field = $fileName; // без этого ошибка

                $model = $this->findModel($model->id);

                if ($field == 'file')
                    $model->image = $fileName;
                if ($field == 'file_logo')
                    $model->logo = $fileName;

                $model->save();

                // Thumbnail
                $this->attachThumbnail($model->id, $fileName, $fileNameThumbs);
            }
        }
    }


    /**
     * @param $id - Model ID
     * @param $fileName - имя сгенерированного оригинального файла
     * @param $fileNameThumbs - имя сгенерированного файла thumbnail
     */
    public function attachThumbnail($id, $fileName, $fileNameThumbs)
    {
        $thumb_w = Yii::$app->params['thumbnails']['width'];
        $thumb_h = Yii::$app->params['thumbnails']['height'];

        $dir = Yii::getAlias('@images'). '/hotels/' . $id . '/';
        $this->createDirectory($dir . 'thumbs');

        // Для ресайза фотки до 800x800px по большей стороне надо обращаться к функции Box() или widen,
        // так как в обертках доступны только 5 простых функций: crop, frame, getImagine, setImagine, text, thumbnail, watermark
        $photo = Image::getImagine()->open($dir . $fileName);
        $photo->thumbnail(new Box(800, 800))->save($dir . $fileName, ['quality' => 100]);
        //$imagineObj = new Imagine();
        //$imageObj = $imagineObj->open(\Yii::$app->basePath . $dir . $fileName);
        //$imageObj->resize($imageObj->getSize()->widen(400))->save(\Yii::$app->basePath . $dir . $fileName);
        Image::thumbnail($dir . $fileName, $thumb_w, $thumb_w)
            ->save(Yii::getAlias($dir .'thumbs/'. $fileNameThumbs), ['quality' => 100]);
    }


    public function createDirectory($path)
    {
        //$filename = "/folder/{$dirname}/";
        if (file_exists($path)) {
            //echo "The directory {$path} exists";
        } else {
            mkdir($path, 0775, true);
            //echo "The directory {$path} was successfully created.";
        }
    }
}
