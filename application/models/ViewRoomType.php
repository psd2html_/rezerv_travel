<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "view_room_type".
 *
 * @property string $id
 * @property string $name
 * @property integer $sort
 *
 * @property RoomType[] $roomTypes
 */
class ViewRoomType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view_room_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'sort' => Yii::t('app', 'параметр сортировки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomTypes()
    {
        return $this->hasMany(RoomType::className(), ['view_room_type_id' => 'id']);
    }
}
