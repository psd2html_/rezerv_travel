<?php

$params = require(__DIR__ . '/params.php');

if (file_exists(__DIR__ . '/db-local.php'))
	$db = require(__DIR__ . '/db-local.php');
else
	$db = require(__DIR__ . '/db.php');

Yii::setAlias('@web', realpath(dirname(__FILE__).'/../../web/'));


$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',

    'aliases' => [
        '@images' => 'uploads/images/',
    ],

    //'defaultRoute' => '/user/security/login',

    'components' => [
        'db' => $db,
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'hWFHvI0Yd7MOKibCgp8C2Di0z35I68U7',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app'       => 'app.php',
                    ],
                ],
                'user' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'user'       => 'user.php',
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'flushInterval' => 1000,
            'targets' => [
//                [
//                    'class' => 'yii\log\DbTarget',
//                    //'levels' => ['error', 'warning', 'info', 'trace'],
//                    'levels' => ['trace'],
//                    //'exportInterval' => 1,
//                    //'categories' => ['yii\db\*'],
//                    //'logVars' => ['_SERVER'],
//                    'prefix' => function ($message) {
//                        $user = Yii::$app->user->id;
//                        $userID = Yii::$app->user->id ? Yii::$app->user->id : '-';
//                        $ip = $_SERVER['REMOTE_ADDR'];
//                        return "[$ip][$userID]";
//                    }
//                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],


//        'user' => [
//            'identityClass' => 'app\models\User',
//            'enableAutoLogin' => true,
//        ],

//        'user' => [
//            'class' => 'app\components\User',
//            'identityClass' => 'dektrium\user\models\User',
//        ],

        // yii2-admin
//        'user' => [
//            'identityClass' => 'mdm\admin\models\User',
//            'loginUrl' => ['admin/user/login'],
//        ],



        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require __DIR__ . DIRECTORY_SEPARATOR . 'rules.php',
        ],

        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        'js/jquery-3.2.1.min.js',
                        'js/jquery-ui.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    //'css' => [],
                ],
            ],
        ],

        'authManager' => [
            //'class' => 'yii\rbac\PhpManager', // or use 'yii\rbac\DbManager'
            //'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
            'class' => 'dektrium\rbac\components\DbManager',
        ],


        // my
        'rbac' =>['class'=>'app\helpers\H_Rbac'],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/yii2-user-module'
                ],
            ],
        ],

    ],

//    'as access' => [
//        'class' => 'mdm\admin\components\AccessControl',
//        'allowActions' => [
//            'site/*',
//            'admin/*',
//            //'some-controller/some-action',
//            // The actions listed here will be allowed to everyone including guests.
//            // So, 'admin/*' should not appear here in the production, of course.
//            // But in the earlier stages of your development, you may probably want to
//            // add a lot of actions here until you finally completed setting up rbac,
//            // otherwise you may not even take a first step.
//        ]
//    ],

    'modules' => [

        // yii2-admin ext.
//        'admin' => [
//            'class' => 'mdm\admin\Module',
//        ],


        // yii2-user ext.
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['superadmin'],
            //'layout' => '@app/views/yii2-user-module/layout',
            'layout' => '@app/views/layouts/main',




            'controllerMap' => [
                'security' =>
                    [
                        // переопределяем класс
                        'class' => 'app\controllers\user\SecurityController',
                        // Вешаем свой обработчик на тригер/событие
                        'on afterLogin' => function ($event) {
                            app\helpers\H_Logs::AddLog('Пользователь авторизовался');
                        },
                        'on beforeLogout' => function ($event) {
                            app\helpers\H_Logs::AddLog('Пользователь вышел');
                        },
                    ],
                'registration' =>
                    [
                        'class' => 'app\controllers\user\RegistrationController',
                        'on afterRegister' => function ($event) {
                            app\helpers\H_Logs::AddLog('Пользователь зарегистрировался');
                        }
                    ],
                'recovery' =>
                    [
                        'class' => 'app\controllers\user\RecoveryController',
                        'on afterRequest' => function($event) {
                            app\helpers\H_Logs::AddLog('Восстановление пароля');
                        }
                    ]
            ],

        ],

        'rbac' => 'dektrium\rbac\RbacWebModule',
    ],



    'params' => $params,
];



if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
