/* DATEPICKER */
//инициализация скриптов календаря
function datepickerInit() {

    var $calendarWrapper = $('#calendar_wrapper');
    var $calendar        = $('#calendar');

    var monthArrayVal   = ['','января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
    var monthArray      = ['','Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
    var monthShortArray = ['','янв','фев','мар','апр','мая','июн','июл','авг','сен','окт','ноя','дек'];

    //дни недели
    var weekdayArray = ['','Пн','Вт','Ср','Чт','Пт','Сб','Вс'];

    var now = moment();
    moment.locale('ru');

    var currentDay   = now.format('D'); 
    var currentMonth = now.format('M'); 
    var currentYear  = now.format('gggg');
    var countMonth   = 12;
    var i            = 0;
    var maxNight     = 100;

    var startDate = now.subtract(countMonth-9, 'month');
    var startDay = 1;
    var startMonth = startDate.format('M');
    var startYear = startDate.format('gggg');

    //закрытие календаря
    $calendarWrapper.find('.close').on('click', function(){
        hideCalendar();

        return false;
    });

    //показ календаря
    $('#date_in_out').on('click', function() {
        showCalendar();
        
        return false;
    });
    

    $calendar
        .on('click', '.day', function() {
            if (!$(this).hasClass('null')) {
                var $dayActive = $calendar.find('.day.active');

                //если уже выбран период обнуляем его и выбираем дату заезда
                if ($dayActive.length == 2) {
                    $calendar.find('.day').removeClass('active start finish disable period');
                    $(this).addClass('active start');

                    //получаем значения даты заезда и устанавливаем их
                    var dateIn     = $(this).data('date');
                    var dateTextIn = $(this).data('datetext');
                    setValue('in', dateIn, dateTextIn);

                    //обнуляем значение даты выезда
                    var dateOut     = '';
                    var dateTextOut = '';
                    setValue('out', dateOut, dateTextOut);

                    //ограничиваем выбор кол-ва ночей
                    disableDate();

                } else {

                    if ( !$(this).hasClass('active') && !$(this).hasClass('disable') ) {

                        var $dayActive = $calendar.find('.day.active');

                        //если есть выбранный день, выбираем дату выезда
                        if ($dayActive.length == 1) {
                            var numCurrent = $dayActive.attr('rel')*1;
                            var numClick   = $(this).attr('rel')*1;

                            if (numClick > numCurrent) {
                                $(this).addClass('active finish');

                                //выделяем выбранный период
                                addPeriod();
                                //$('#calendar_select .text').append(' - '+$(this).data('datetext'));
                                var dateOut     = $(this).data('date');
                                var dateTextOut = $(this).data('datetext');
                                setValue('out', dateOut, dateTextOut);
                                
                                setTimeout(function(){ hideCalendar(); }, 0);

                            } else {
                                $calendar.find('.day').removeClass('active start disable');
                                $(this).addClass('active start');
                                
                                var dateIn     = $(this).data('date');
                                var dateInText = $(this).data('datetext');
                                setValue('in', dateIn, dateInText);

                                disableDate();
                            }

                        } else {
                            $(this).addClass('active start');

                            var dateIn     = $(this).data('date');
                            var dateInText = $(this).data('datetext');
                            setValue('in', dateIn, dateInText);

                            disableDate();
                        }
                    }
                }
            }
            

            return false;
        })
        .on('mouseover', '.day', function(){
            var $dayActive = $calendar.find('.day.active');
            if($dayActive.length == 1 && !$(this).hasClass('disable')){
                var start = $calendar.find('.day.active.start').attr('rel')*1+1;
                var finish = $(this).attr('rel')*1+1;
                console.log(start+'|'+finish);
                if(finish > start){
                    $calendar.find('.day').not('.null').slice(start, finish).addClass('hover');
                };
            };
        })
        .on('mouseout', '.day', function(){
            $calendar.find('.day.hover').removeClass('hover');
        });

    //формирование HTML каледаря календаря
    function create(day, month, year) {
        var startDate = day;

        if (countMonth > 0) {
            var monthDays = moment(year+'-'+month, 'YYYY-M').daysInMonth();
            var monthHTML = '<div class="month-wrap"><p class="month">'+monthArray[month]+' '+year+'</p>';

            day = 1;
            while (day <= monthDays) {
                var weekday = moment(year+'-'+month+'-'+day,'YYYY-M-D').format('E');

                if ( day == 1 || weekday == 1 ) {
                    monthHTML +='<div class="week">';
                }

                if (day < 10) {
                    var dayFull = '0'+day;
                } else {
                    var dayFull = day;
                }

                if (month < 10 ) {
                    var monthFull = '0'+month;
                } else {
                    var monthFull = month;
                }

                var fullDate = dayFull+'.'+monthFull+'.'+year;
                //var textDate = '<strong>'+day+'</strong> '+monthArrayVal[month];
                //var textDate = day+'|'+monthShortArray[month]+'|'+weekdayArray[weekday];
                var textDate = fullDate;
                //var textDate = dayFull+'.'+monthFull+'.'+year;

                //проверка для формирования прошедших дат текущего месяца
                var prevDateClass = '';
                //console.log(day+' '+startDate+' ||| '+month+' == '+currentMonth);
                /*if (startDate > 1 && month == currentMonth && day < startDate) {
                    prevDateClass = 'null';
                    i = -1;
                }*/

                monthHTML += '<div class="day d'+weekday+' '+prevDateClass+'" rel="'+i+'" data-datetext="'+textDate+'" data-date="'+fullDate+'"><span>'+day+'</span></div>';

                if ( day == monthDays || weekday == 7 ) {
                    monthHTML += '</div>';
                    if (day == monthDays) {
                        monthHTML += '</div>';
                    }
                }

                day++;
                i++;
            }

            $calendar.append(monthHTML);

            countMonth--;
            month = month*1+1;

            if (month > 12) {
                month = 1;
                year++;
            }

            create(1, month, year);

        } else {
            var dateIn  = $('#date_in').val();
            var dateOut = $('#date_out').val();

            if (dateIn != '' && dateOut != '') {
                $calendar.find('.day[data-date="'+dateIn+'"]').trigger('click');
                $calendar.find('.day[data-date="'+dateOut+'"]').trigger('click');

            } else {
                $calendar.find('.day[rel=0]').trigger('click');
                $calendar.find('.day[rel=1]').trigger('click');    
            }
            
        }

        /*$('#calendar').jScrollPane({
            autoReinitialise: true
        });*/
    }

    //выделяем выбранный период бронирования
    function addPeriod(){
        $calendar.find('.day').removeClass('disable');

        var start  = $calendar.find('.day.active.start').attr('rel')*1;
        var finish = $calendar.find('.day.active.finish').attr('rel')*1 + 1;

        $calendar.find('.day:not(.null)').slice(start, finish).addClass('period');
    }

    //установка ограничения на колво ночей
    function disableDate(){
        var start = $calendar.find('.day.active.start').attr('rel')*1 + maxNight;
        $calendar.find('.day:not(.null)').slice(start).addClass('disable');
    }

    //показ календаря
    function showCalendar() {
        $calendarWrapper.addClass('open');
    }
    //закрытие календаря
    function hideCalendar(){
        $calendarWrapper.removeClass('open');
    }

    //установка значения даты, на вход:
    //type - тип даты заезд/выезд(in/out)
    //date - значение даты дд.мм.гггг
    //text - значение даты текстовое для html
    function setValue(type, date, text) {
        var $obj;
        var textdate = '';
        if (type == 'in') {
            $obj =  $('#date_in');
            textdate = text+' - ';

        } else {
            $obj =  $('#date_out');

            textdate += $('#date_in_out').val()+text;
        }

        //console.log(textdate);
        //формируем html выбранной даты
        $obj.val(date);
        $('#date_in_out').val(textdate);

        /*
        //получаем количество ночей если выбрано 2 даты
        var countNight = $calendar.find('.day.period').length * 1 - 1;
        if (countNight < 0) {
            countNight = 0;
        }

        var labelNight = declOfNum(countNight, ['ночь', 'ночи', 'ночей']);
        $('#select_date, #calendar_date').find('.nights p').html('<span class="num">'+countNight+'</span> '+labelNight);
        */
    }

    //create(currentDay,currentMonth,currentYear);
    create(startDay, startMonth, startYear);

    $('.field.calendar').on('click', function(){
        if ($calendarWrapper.hasClass('open')) {
            hideCalendar();

        } else {
            showCalendar();
        }

        return false;
    });
}
/* END DATEPICKER */