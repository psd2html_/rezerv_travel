<?php


return [
    'Log In' => 'Войти',
    'Remember me' => 'Запомнить меня',
    'Forgot your password?' => 'Забыли пароль?',
    'Sign in' => 'Авторизация',
    'Sign up' => 'Регистрация',
    'Sign Up' => 'Зарегистрироваться',
    'Invalid login or password' => 'Не верный логин или пароль',
    'Reset' => 'Отправить',
    'Forgot password' => 'Восстановление пароля',
    'Forgot instructions' => 'Введите свой адрес электронной почты, и вам будут отправлены инструкции',
    'Register' => 'Регистрация',
    'Have account already? Please go to' => 'У вас уже есть аккаунт? Тогда',
    'Remember me next time' => 'Запомнить меня',
    '' => '',
    '' => '',

];