<?php

namespace app\controllers\user;

use dektrium\user\controllers\RegistrationController as BaseAdminController;


class RegistrationController extends BaseAdminController
{
    public $layout = '@app/views/yii2-user-module/login_register_layout';
}
