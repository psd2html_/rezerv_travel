<?php

namespace app\controllers\user;

use Yii;
use dektrium\user\controllers\RecoveryController as BaseAdminController;


class RecoveryController extends BaseAdminController
{
    public $layout = '@app/views/yii2-user-module/login_register_layout';
}
